import csv
import time
import urllib
from slugify import slugify

from config import *
from countries import Countries
from search_config import SearchConfig
from tools import tools

c_int = tools.ignore_exception(ValueError)(int)


class ieeexplore(object):
    # CONSTANTS
    url_template = 'http://ieeexplore.ieee.org/search/searchExport.jsp?queryText={0}&ranges={1}_{2}_Year'
    url_template_2020 = 'https://ieeexplore.ieee.org/search/searchresult.jsp?queryText={0}&highlight=true&returnFacets=ALL&returnType=SEARCH&ranges={1}_{2}_Year'

    @staticmethod
    def crawl(search: SearchConfig, keyword: str = None):
        search.set_status('IEEE', 'Starting')
        h.log_line('Crawling IEEE Xplore...')
        search.result_counts[Config.sources['ieee']] = {}
        if keyword:
            ieeexplore._process_keyword(search, keyword)
        else:
            for keyword in search.search_data['keywords']['final']:
                ieeexplore._process_keyword(search, keyword)
        search.set_status('IEEE')
        search.to_pickle()

    @staticmethod
    def _process_keyword(search: SearchConfig, keyword: str):
        ieeexplore._crawl_keyword(search.folder_IEEEXplore, keyword, search.start_year, search.end_year)
        ieeexplore._clean_result(search, keyword)

    @staticmethod
    def _clean_result(search: SearchConfig, keyword=None):
        h.log_line('Cleaning IEEE Xplore results...')
        if keyword:
            ieeexplore._clean_result_keyword(keyword, search)
        else:
            for keyword in search.keywords:
                ieeexplore._clean_result_keyword(keyword, search)

    @staticmethod
    def _crawl_keyword(folder_name, keyword, start_year, end_year):
        keyword_filename = os.path.join(folder_name, slugify(keyword) + '_raw.csv')
        url_keyword = urllib.parse.quote(keyword)
        download_url = ieeexplore.url_template.format(url_keyword, start_year, end_year)
        tools.download_file_wget_external(download_url, keyword_filename)

    @staticmethod
    def get_abstract_citations(web_page_content):
        """Extracts them from the content of a page like https://ieeexplore.ieee.org/document/6690733/"""
        try:
            s = tools.between(web_page_content, 'global.document.metadata=', '</script>').strip().rstrip(';')
        except Exception as e:
            h.log_line('Exception in parsing IEEE article details page #1', e, 'Content:', web_page_content)
            a, c = '', '0'
            return a, c
        try:
            # h.log_line('Segment:', s)
            d = h.from_json(json_string=s)
        except Exception as e:
            if len(s) > 0:
                h.log_line('Exception in parsing IEEE article details page #2', e, 'Content:', web_page_content,
                           'Segment:', s)
            a, c = '', '0'
            return a, c
        try:
            a = d.get('abstract', '')
            # h.log_line('Abstract:', a)
            c = str(d.get('citationCount', '0'))
            if c == '0':
                try:
                    c = str(d.get('metrics', {}).get('citationCountPaper', '0'))
                except:
                    pass
            # h.log_line('citationCount:', c)
            # exit(0)
        except Exception as e:
            h.log_line('Exception in parsing IEEE article details page #3', e, 'Content:', web_page_content, 'Dict:', d)
            a, c = '', '0'
        return a, c

    @staticmethod
    def _clean_result_keyword(keyword, search: SearchConfig = None, src_filename=None):
        if search is None:
            search = SearchConfig()
        if src_filename is None:
            src_filename = os.path.join(search.folder_IEEEXplore, slugify(keyword) + '_raw.csv')
        try:
            data = []
            search.set_result_counts(Config.sources['ieee'])
            with open(src_filename, 'r', encoding='utf8') as fp:
                for i, row in enumerate(csv.reader(fp, delimiter=',')):
                    try:
                        if i % 100 == 0:
                            h.log_line('Processing line:', i, 'in', src_filename)
                        citations = 0
                        if i >= 1:
                            search.result_counts[Config.sources['ieee']]['considered'] += 1
                            if (row[21]):
                                citations += c_int(row[21])
                            if (row[22]):
                                citations += c_int(row[22])
                            if (row[23]):
                                citations += c_int(row[21])
                            vals = row[0], row[10], row[1], str(citations), row[4], row[5], row[15], row[13]
                            vals = (h.remove_tabs(v) for v in vals)
                            title, abstract, authors, number_of_citations, date, year, link, doi = vals
                            authors = authors.replace(';', ',')
                            num_authors = authors.count(',') + 1
                            affil_unknown = ['NA'] * num_authors
                            affil_unknown = '; '.join(affil_unknown)
                            # print(affil_unknown, row[2])
                            s_affil = None if row[2].strip() == affil_unknown else row[2]
                            affiliations, num_diff_affiliations, countries, num_diff_countries = Countries.process_affiliations(
                                s_affil)
                            # print(i, num_diff_affiliations, countries, num_diff_countries, affiliations, row[2])
                            # continue
                            year = year[:4]
                            # displayPublicationTitle, publicationTitle
                            publication_title = row[3]
                            if len(year) == 4 and not (search.start_year <= int(year) <= search.end_year):
                                search.result_counts[Config.sources['ieee']]['invalid_years'] += 1
                                h.log_line('Invalid year:', i, year, search.start_year, search.end_year, 'in file:',
                                           src_filename)
                                continue

                            if h.is_empty(doi):
                                _doi = tools.simplify_url(link)
                                if len(_doi) > 0:
                                    doi = _doi
                                else:
                                    doi = h.remove_nonalphanumeric(title, '')
                            abstract = h.remove_tabs(abstract)
                            if len(abstract) < Config.abstract_min_length:
                                filename_page = tools.html_filename(link, search.folder_IEEE_Articles)
                                h.log_line('No abstract', i, 'attempting to get more details from:', link, 'in file:',
                                           filename_page)
                                page, final_url = tools.download_page(link, filename_page, True)
                                if final_url != link:
                                    if search.exists_url(final_url, Config.sources['ieee']):
                                        search.result_counts[Config.sources['ieee']]['duplicates'] += 1
                                        continue
                                    filename_page = tools.html_filename(final_url, search.folder_IEEE_Articles)
                                    page, final_url = tools.download_page(final_url, filename_page, True)
                                if 'ieeexplore.ieee.org/document' in final_url or 'ieeexplore.ieee.org/stamp/stamp.jsp?arnumber=' in final_url:
                                    abstract, citations = ieeexplore.get_abstract_citations(page)
                                    abstract = h.remove_tabs(abstract)
                                    h.log_line('Trying to get abstract from:', final_url, 'Success:',
                                               len(abstract) > Config.abstract_min_length)

                            if len(abstract) < Config.abstract_min_length:
                                search.result_counts[Config.sources['ieee']]['incomplete_data'] += 1
                                h.log_line('Incomplete data (abstract small):', i, len(abstract), '<',
                                           Config.abstract_min_length, 'Abstract:', abstract, 'in file:', src_filename)
                                continue
                            if h.is_empty(authors):
                                search.result_counts[Config.sources['ieee']]['incomplete_data'] += 1
                                h.log_line('Incomplete data (authors empty). in file:', src_filename)
                                continue
                            if search.exists_doi(doi, Config.sources['ieee']) or search.exists_url(link, Config.sources['ieee']):
                                h.log_line('Exists doi or url:', i, doi, link, src_filename)
                                search.result_counts[Config.sources['ieee']]['duplicates'] += 1
                                continue
                            if num_diff_countries > num_authors:
                                num_diff_countries = num_authors
                            s_emails = ''
                            fields = [doi, link, title, authors, date, year, citations, abstract, keyword,
                                      Config.sources['ieee'], publication_title, affiliations, num_diff_affiliations,
                                      countries, num_diff_countries, num_authors, s_emails]
                            clean_fields = [h.remove_tabs(s) for s in fields]
                            data.append(clean_fields)
                    except Exception as e:
                        h.log_line('Line:', i + 1, e, 'Data:', row)
            dst_filename = os.path.join(search.folder_IEEEXplore, slugify(keyword) + '_links.csv')
            search.write_results(data, dst_filename)

        except Exception as ex:
            h.log_line('Error while processing:', keyword, 'sql_filename:', src_filename, 'Exception:', ex)
