class Config(object):
    # CONFIGURATION
    min_properties_relevant = 3
    max_papers_springer = 5000
    max_papers_acm = 5000

    max_article_authors = 100
    max_article_affiliations = 15
    number_of_synonyms_word_phrase = 3
    number_of_pages_springer = int(max_papers_springer / 20)
    number_of_pages_acm = int(max_papers_acm / 20)
    browser_delay_time_acm = 60  # Selenium - time for browser to load dynamic content
    browser_delay_time_ncbi = 60  # Selenium - time for browser to load dynamic content

    browser_delay_time_ieeexplore = 60  # Selenium - time for browser to load dynamic content
    max_download_time_selenium = 2 * 3600  # seconds

    abstract_min_length = 10
    abstract_export_max_length = 3000

    DATA_FOLDER = 'data'
    ACM_FOLDER = 'acm'
    NCBI_FOLDER = 'ncbi'
    SPRINGER_FOLDER = 'springer'
    IEEEXPLORE_FOLDER = 'ieeexplore'
    sources = {'springer': 'Springer', 'acm': 'ACM', 'ieee': 'IEEE Xplore', 'ncbi': 'NCBI', 'pubmed': 'NCBI:PubMed'}
    search_stats_header = ['considered', 'invalid_years', 'incomplete_data', 'duplicates', 'remaining', 'relevant']
    # CONSTANTS
    keywords_result_header = ['doi', 'link', 'title', 'authors', 'date', 'year', 'citations',
                              'abstract', 'keyword', 'source', 'publication_title', 'affiliations',
                              'num_diff_affiliations', 'countries', 'num_diff_countries', 'num_authors', 'emails']
    keywords_result_header_extended = keywords_result_header + ['abstract_stemmed', 'bibtex_id']
    keywords_summary_header = keywords_result_header + ['bib_key', 'num_property_groups', 'num_properties',
                                                        'property_groups', 'properties']
