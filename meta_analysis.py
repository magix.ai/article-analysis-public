import matplotlib as mpl

mpl.use('Agg')
from config import *
import helpers as h
from search_config import SearchConfig
from tools import tools
from bibtexparser.bwriter import BibTexWriter
from bibtexparser.bibdatabase import BibDatabase
from pandas import DataFrame
from countries import Countries
import shutil
from output_results import plot_all
import pandas as pd
import os, time, json
from acm import acm
from ieeexplore import ieeexplore
from ncbi import ncbi
from springer import springer


def _merge_crawled_articles(search: SearchConfig):
    try:
        df = pd.read_csv(search.file_articles_all, sep='\t', header=0)
        df = df.astype(str)
    except:
        h.log_line('Merging crawling results...')
        search.set_status('Merge Scraped Articles', 'Starting')
        data = []
        # for source, sql_filename in search.result_files:
        for filename in search.result_files:
            if os.path.isfile(filename) and os.stat(filename).st_size > 0:
                cdata, hd = h.read_csv_file(filename, 'str', True, separator='\t', has_header=True)
                # duplicates should be already removed during crawling
                data.extend(cdata)

        data = [Config.keywords_result_header] + data
        h.write_csv_file(data, search.file_articles_all, separator='\t')
        df = pd.read_csv(search.file_articles_all, sep='\t', header=0)
    h.log_line('Finished merging articles')
    search.set_status('Merge Scraped Articles')
    search.to_pickle()
    return df


def stem_articles(search: SearchConfig, df: pd.DataFrame = None):
    h.log_line('Tokenizing articles and generating bibtex...')
    search.set_status('Tokenizing articles', 'Starting')
    if not os.path.isfile(search.file_articles_stemmed) or not os.path.isfile(search.filename_bibtex_temp):
        if df is None:
            df = pd.read_csv(search.file_articles_all, sep='\t')
        df = df.astype(str)

        bibtex_db = BibDatabase()
        bibtex_writer = BibTexWriter()

        bibtex_doi_ids = {}
        bibtex_id_counter = 0
        abstracts_stemmed = []
        bibtex_ids_list = []
        bibtex_ids_dict = {}
        valid_rows=[]
        for i, row in df.iterrows():
            try:
                authors_tokens = tools.word_tokenizer(row.authors)
                authors_key = ''
                for auth in authors_tokens:
                    authors_key += auth
                    if len(authors_key) > 5:
                        break
                # Generate google scholar-like keys: lawrence1999digital
                title_token = tools.word_tokenizer(row.title)
                if len(title_token) == 0:
                    title_token = ['']
                bibtex_id_base = str(authors_key) + str(row.year) + str(title_token[0])
                bibtex_id = h.remove_nonalphanumeric(bibtex_id_base.lower(), '').replace(' ', '')
                while bibtex_id in bibtex_ids_dict:
                    bibtex_id_counter += 1
                    bibtex_id = bibtex_id_base + str(bibtex_id_counter)

                bibtex_db.entries = [
                    dict(title=str(row.title), author=str(row.authors), year=str(row.year), ID=bibtex_id,
                         publisher=row.source.replace(' Xplore', '').replace('NCBI:', ''),
                         journal=str(row.publication_title).replace('nan', ''),
                         doi=str(row.doi), url=str(row.link), ENTRYTYPE='article')]
                cbibtex_content = bibtex_writer.write(bibtex_db)
                bibtex_ids_dict[bibtex_id] = cbibtex_content
                bibtex_doi_ids[row.doi] = bibtex_id
                abstract_stemmed = tools.stem_text(row.abstract + ' ' + row.title, search.apply_stemming)
                abstracts_stemmed.append(abstract_stemmed)
                bibtex_ids_list.append(bibtex_id)
                valid_rows.append(i)
            except Exception as e:
                h.log_line('Invalid row', i, len(row), row, e)
                # abstracts_stemmed.append('INVALID ROW')
                # bibtex_ids_list.append(f'invalid_row_{i}')
        df = df.iloc[valid_rows]
        df['abstract_stemmed'] = abstracts_stemmed
        df['bibtex_id'] = bibtex_ids_list
        df.replace(to_replace=[r"\\t|\\n|\\r", "\t|\n|\r"], value=["", ""], regex=True, inplace=True)
        df.to_csv(search.file_articles_stemmed, sep='\t', index=False)
        h.to_pickle((bibtex_ids_dict, bibtex_doi_ids), search.filename_bibtex_temp)
        search.to_pickle()
    else:
        df = pd.read_csv(search.file_articles_stemmed, sep='\t')
        df = df.astype(str)
        bibtex_ids_dict, bibtex_doi_ids = h.from_pickle(search.filename_bibtex_temp)
    search.set_status('Tokenizing articles')
    h.log_line('Finished tokenizing articles. Remaining articles:', len(df))
    return bibtex_ids_dict, bibtex_doi_ids, df


def _generate_bibtex(search: SearchConfig, bibtex_ids, bibtex_doi_ids, retained_dois):
    search.set_status('Generate bibtex', 'Starting')
    with open(search.filen_articles_bib, 'w', encoding='utf8') as bibfile:
        for i, doi in enumerate(retained_dois):
            try:
                bib_entry = bibtex_ids[bibtex_doi_ids[doi]]
                bibfile.write(bib_entry)
            except Exception as e:
                h.log_line('Invalid entry:', i, type(doi), doi, 'Exception:', e)
    search.set_status('Generate bibtex')


def process_articles(search: SearchConfig, df: pd.DataFrame = None):
    search.set_status('Process articles', 'Starting')
    if df is None:
        _merge_crawled_articles(search)
    bibtex_ids, bibtex_doi_ids, df = stem_articles(search, df)

    search.move_old_results()
    hd_output_details = Config.keywords_summary_header
    # ['num_property_groups', 'num_properties', 'property_groups', 'properties']
    property_groups = sorted(search.property_groups)
    hd_output_property_groups = Config.keywords_summary_header[:-2] + property_groups
    hd_output_properties = Config.keywords_summary_header[:-2] + search.properties_major

    # output_details = []
    # output_details_fil = []
    # output_property_groups = []
    output_property_groups_fil = []
    # output_properties = []
    # output_properties_fil = []
    aggr_prop_group_year = {}
    aggr_prop_year = {}
    dois = set()
    links = set()
    years = set()
    retained_dois = set()
    source_year = {}
    per_year = {}
    sources = set()
    per_keyword = {}
    keyword_source = {}
    keyword_year = {}
    keywords = set()
    country_affil_authors = dict()
    graph_data = [['doi', 'affiliations', 'countries', 'properties']]
    for source in search.result_counts:
        for metric in ['remaining', 'relevant']:
            search.result_counts[source][metric] = 0
    for i, row in df.iterrows():
        if h.is_emptyz(row.year) or not search.start_year <= float(row.year) <= search.end_year:
            h.log_line(f'Skipping. Year Issue: {search.start_year}<={float(row.year)}<={search.end_year}')
            continue

        if float(row.num_diff_affiliations) > Config.max_article_affiliations:
            h.log_line('Skipping. Article has too many affiliations:', row.num_diff_affiliations)
            continue
        if float(row.num_authors) > Config.max_article_authors:
            h.log_line('Skipping. Article has too many authors:', row.num_authors)
            continue
        if row.doi in dois or row.link in links:
            h.log_line('Skipping. Article with same doi exists:', row.doi)
            continue
        dois.add(row.doi)
        keywords.add(row.keyword)
        links.add(row.link)
        matched_property_groups = {}
        matched_properties_flat = set()
        years.add(row.year)
        sources.add(row.source)
        source_year.setdefault((row.source, row.year), [0, 0])
        source_year[(row.source, row.year)][0] += 1
        keyword_year.setdefault((row.keyword, row.year), [0, 0])
        keyword_year[(row.keyword, row.year)][0] += 1
        doi_sources = search.processed_dois.get(row.doi, set())
        doi_sources.add(row.source)  # should be there already, but just in case
        for doi_source in doi_sources:
            keyword_source.setdefault((row.keyword, doi_source), [0, 0])
            keyword_source[(row.keyword, doi_source)][0] += 1
        per_year.setdefault(row.year, [0, 0])
        per_year[row.year][0] += 1

        per_keyword.setdefault(row.keyword, [0, 0])
        per_keyword[row.keyword][0] += 1

        abstract_stemmed_tup = tools.text_to_tuple(row.abstract_stemmed)
        properties_mandatory_satisfied = True
        for properties_mandatory_OR_group in search.properties_mandatory_stemmed:
            mandatory_property_current = False
            for mandatory_property_OR in properties_mandatory_OR_group:
                if Countries.is_subsequence(mandatory_property_OR, abstract_stemmed_tup):
                    mandatory_property_current = True
                    break
            properties_mandatory_satisfied = properties_mandatory_satisfied and mandatory_property_current
            if not properties_mandatory_satisfied:
                break
        if properties_mandatory_satisfied:
            for st_prop_tup, prop_groups in search.property_stem_lookup_tuple.items():
                if Countries.is_subsequence(st_prop_tup, abstract_stemmed_tup):
                    cur_property = search.stem_source_lookup_tuple[st_prop_tup]
                    for k, major_property in enumerate(search.property_synonym_lookup[cur_property]):
                        aggr_prop_year.setdefault((major_property, row.year), 0)
                        aggr_prop_year[(major_property, row.year)] += 1
                        matched_properties_flat.add(major_property)
                        for prop_group in search.property_lookup[major_property]:
                            matched_property_groups.setdefault(prop_group, set())
                            matched_property_groups[prop_group].add(major_property)
        for prop_group in matched_property_groups:
            aggr_prop_group_year.setdefault((prop_group, row.year), 0)
            aggr_prop_group_year[(prop_group, row.year)] += 1
        # found_prop = []
        found_prop_ext = []
        found_prop_expanded = []
        for prop_group in property_groups:
            p = matched_property_groups.get(prop_group, set())
            pm = ','.join(sorted(p))
            found_prop_ext.append(pm)

        for prop in search.properties_major:
            if prop in matched_properties_flat:
                found_prop_expanded.append('Yes')
            else:
                found_prop_expanded.append('')

        record_keys = Config.keywords_result_header_extended[:-2] + [Config.keywords_result_header_extended[-1]]
        # record_simple = record[:-2] + [record[-1]]
        record_simple = row[record_keys]
        record_simple = [str(r)[:Config.abstract_export_max_length] for r in record_simple]
        row_pg = list(record_simple) + [len(matched_property_groups), len(matched_properties_flat)] + found_prop_ext

        if properties_mandatory_satisfied:
            if len(matched_properties_flat) >= search.relevant:
                retained_dois.add(row.doi)
                # output_details_fil.append(row_details)
                output_property_groups_fil.append(row_pg)
                # output_properties_fil.append(row_p)
                search.result_counts.setdefault(row.source, {})
                search.result_counts[row.source].setdefault('relevant', 0)
                search.result_counts[row.source]['relevant'] += 1
                source_year[(row.source, row.year)][1] += 1
                keyword_year[(row.keyword, row.year)][1] += 1
                for doi_source in doi_sources:
                    keyword_source[(row.keyword, doi_source)][1] += 1
                per_year[row.year][1] += 1
                per_keyword[row.keyword][1] += 1

                country_affil_authors.setdefault((row.num_diff_countries, row.num_diff_affiliations, row.num_authors),
                                                 0)
                country_affil_authors[(row.num_diff_countries, row.num_diff_affiliations, row.num_authors)] += 1
            if len(matched_properties_flat) >= max(2, search.relevant):
                # no point of graph if there aren't at least two properties
                str_major_properties = ','.join(matched_properties_flat)
                graph_data.append([row.doi, row.affiliations, row.countries, str_major_properties])
        else:
            h.log_line('Row not satisfied. Found:', found_prop_ext, 'Full row:', row_pg)
        if i % 100 == 0:
            search.set_status('Process articles', f'Line {i} of {len(df)}')
            h.log_line('Process articles. Line', i, 'of', len(df), 'in file:', search.file_articles_stemmed)
        search.result_counts.setdefault(row.source, {})
        search.result_counts[row.source].setdefault('remaining', 0)
        search.result_counts[row.source]['remaining'] += 1

    search.to_pickle()
    _generate_bibtex(search, bibtex_ids, bibtex_doi_ids, retained_dois)

    h.write_csv_file(graph_data, search.file_results_graph_data, separator='\t')
    # h.csv_to_excel(search.file_results_graph_data)
    years = sorted(years)
    sources = sorted(sources)

    result_aggr_source = [['source'] + Config.search_stats_header]
    for source, d in search.result_counts.items():
        result_aggr_source.append([source] + [d.get(stat, 0) for stat in Config.search_stats_header])
    h.write_csv_file(result_aggr_source, search.file_results_aggr_source, separator='\t')

    result_aggr_source_year = [['source', 'year', 'analyzed_articles', 'relevant_articles']]
    result_aggr_source_year += sorted([tuple(list(k) + v) for k, v in source_year.items()])
    result_aggr_year = [['year', 'analyzed_articles', 'relevant_articles']]
    result_aggr_year += sorted([tuple([year] + v) for year, v in per_year.items()])

    result_aggr_source_year_pivot_relevant = [['source'] + years]
    for source in sources:
        result_aggr_source_year_pivot_relevant.append(
            [source] + [source_year.get((source, year), [0, 0])[1] for year in years])

    result_aggr_keyword_year_pivot_relevant = [['keyword'] + years]
    for keyword in keywords:
        result_aggr_keyword_year_pivot_relevant.append(
            [keyword] + [keyword_year.get((keyword, year), [0, 0])[1] for year in years])

    result_aggr_keyword_source_pivot_relevant = [['keyword'] + sources]
    for keyword in keywords:
        result_aggr_keyword_source_pivot_relevant.append(
            [keyword] + [keyword_source.get((keyword, source), [0, 0])[1] for source in sources])

    result_aggr_source_year_pivot_analyzed = [['source'] + years]
    for source in sources:
        result_aggr_source_year_pivot_analyzed.append(
            [source] + [source_year.get((source, year), [0, 0])[0] for year in years])

    prop_groups = sorted(search.property_groups.keys())
    result_aggr_prop_group_year = [['property_group', 'year', 'article_count']]
    result_aggr_prop_group_year += [(pg, y, aggr_prop_group_year.get((pg, y), 0)) for pg in prop_groups for y in
                                    years]

    result_aggr_prop_group_year_pivot = [['property_group'] + years]
    for property_group in prop_groups:
        l = [property_group] + [aggr_prop_group_year.get((property_group, y), '0') for y in years]
        result_aggr_prop_group_year_pivot.append(l)

    result_aggr_prop_year = [['property_group', 'property', 'year', 'article_count']]
    result_aggr_prop_year_pivot = [['property_group', 'property'] + years]
    for pg in sorted(search.property_groups.keys()):
        for p in sorted(search.property_groups[pg]):
            yd = [pg, p[0]]
            for y in years:
                cnt = aggr_prop_year.get((p[0], y), 0)
                result_aggr_prop_year.append((pg, p[0], y, cnt, 0))
                yd.append(cnt)
            result_aggr_prop_year_pivot.append(yd)

    lcountry_affil_authors = [['num_countries', 'num_affiliations', 'num_authors', 'relevant_articles']] + sorted(
        [list(k) + [v] for k, v in country_affil_authors.items()])

    # aggregated
    h.write_csv_file(lcountry_affil_authors, search.file_results_aggr_num_countries_affiliations_authors,
                     separator='\t')
    h.csv_to_excel(search.file_results_aggr_num_countries_affiliations_authors)

    h.write_csv_file(result_aggr_source_year, search.file_results_aggr_source_year, separator='\t')
    h.write_csv_file(result_aggr_year, search.file_results_aggr_year, separator='\t')
    h.write_csv_file(result_aggr_source_year_pivot_relevant, search.file_results_aggr_source_year_pivot_relevant,
                     separator='\t')
    h.csv_to_excel(search.file_results_aggr_source_year_pivot_relevant)
    h.write_csv_file(result_aggr_keyword_year_pivot_relevant, search.file_results_aggr_keyword_year_pivot_relevant,
                     separator='\t')
    h.csv_to_excel(search.file_results_aggr_keyword_year_pivot_relevant)
    h.write_csv_file(result_aggr_keyword_source_pivot_relevant,
                     search.file_results_aggr_keyword_source_pivot_relevant,
                     separator='\t')
    h.csv_to_excel(search.file_results_aggr_keyword_source_pivot_relevant)
    h.write_csv_file(result_aggr_prop_group_year, search.file_results_aggr_property_group_year, separator='\t')
    h.write_csv_file(result_aggr_prop_group_year_pivot, search.file_results_aggr_property_group_year_pivot,
                     separator='\t')
    h.csv_to_excel(search.file_results_aggr_property_group_year_pivot)
    h.write_csv_file(result_aggr_prop_year, search.file_results_aggr_property_year, separator='\t')
    h.write_csv_file(result_aggr_prop_year_pivot, search.file_results_aggr_property_year_pivot,
                     separator='\t')
    h.csv_to_excel(search.file_results_aggr_property_year_pivot)

    sort_columns = ['num_property_groups', 'num_properties', 'year', 'source', 'citations', 'title']
    ascending_flag = [False, False, False, True, False, True]
    df_output_property_groups_fil = DataFrame(output_property_groups_fil, columns=hd_output_property_groups)
    df_output_property_groups_fil.sort_values(sort_columns, ascending=ascending_flag, inplace=True)
    df_output_property_groups_fil.to_csv(search.file_results_articles_filtered, sep='\t', index=False)
    h.csv_to_excel(search.file_results_articles_filtered)

    # COPY used files
    shutil.copyfile(search.filename_json, search.file_searched_keywords_final)

    plot_all(search)
    search.set_status('Process articles')


def process_keywords(folder_name=None, start_year=None, end_year=None, folder_repo=None,
                     relevant=Config.min_properties_relevant, capitalize=False,
                     ask_to_replace=True, add_lin_coef=False, stem=True, plot_type='pdf'):
    t0 = time.time()
    h.log_line('Starting\n\n')

    # ENTER CURRENT WORKING FOLDER NAME
    while not folder_name:
        folder_name = input('Enter working folder name (absolute or relative path): ')
        h.log_line('\n')

    # ENTER KEYWORDS AND PROPERTIES IN A JSON FILE
    search = SearchConfig(folder_name, start_year=start_year, end_year=end_year, folder_repo=folder_repo,
                          relevant=relevant, add_lin_coef=add_lin_coef, apply_stemming=stem, capitalize=capitalize,
                          plot_type=plot_type)
    search.save_keywords(input_type="example")
    h.log_line('Enter search keywords and properties in:', search.filename_initial)
    if ask_to_replace:
        # h.open_file_externally(search.filename_initial)
        input("Press Enter when ready to continue...")
    search.reload_keywords()
    # PRINT TO CONSOLE KEYWORDS AND SYNONYMS PROPOSED
    h.log_line('Final search keywords: ')
    h.log_line(json.dumps(search.keywords, indent=4, sort_keys=True))
    h.log_line('\n')
    h.log_line('Final abstract properties: ')
    h.log_line(json.dumps(search.properties, indent=4, sort_keys=True))
    h.log_line('\n')
    search.stem_properties()
    h.log_line('Property stemming finished')
    h.log_line('Final abstract stemmed properties: ')
    h.log_line(json.dumps(search.properties_stemmed, indent=4, sort_keys=True))
    duration = time.time() - t0
    h.log_line('Finished. Duration:', duration)
    search.to_pickle()
    return search


def crawl_and_analyze(search: SearchConfig):
    t0 = time.time()
    h.log_line('Starting' + search.folder_data + '\n\n')
    #if search.is_library_enabled("PubMed"): ncbi.crawl(search, library='PubMed')
    #if search.is_library_enabled("PMC"): ncbi.crawl(search, library='PMC')
    #if search.is_library_enabled("IEEE"): ieeexplore.crawl(search)
    if search.is_library_enabled("Springer"): springer.crawl(search)
    #if search.is_library_enabled("ACM"): acm.crawl(search)

    # process results
    process_articles(search)

    duration = time.time() - t0
    h.log_line('Finished. Duration:', duration)