from browser_emulate import *
import helpers as h
import csv
from selenium.webdriver.common.keys import Keys
from time import sleep
from selenium.common.exceptions import NoSuchElementException
from tools import tools  
from lxml import html  
from urllib.parse import urlparse
import pathlib


def download_file(url, download_dir, filename=None, return_string=False):
	if filename is None:
		filename = tools.html_filename(url)
	final_url = url
	if os.path.isfile(filename) and os.stat(filename).st_size > 0:
		h.log_line('File exists:', filename, 'Skipping download of', url)
		with open(filename, 'r', encoding="utf-8") as file:
			s = file.read()
	else:
		browser = create_browser(download_dir)
		browser.get(url)
		sleep(3)
		s = browser.page_source
		with open(filename,'w+', encoding="utf-8") as f:
			f.write(s)
		browser.close()
	if return_string:
		web_page = s 
	else:
		web_page = html.fromstring(s)
	return web_page, final_url



url='https://dl.acm.org/results.cfm?query=(vladimir%20trajkovik)&within=owners.owner=HOSTED&filtered=&dte=2017&bfr=2018'
download_dir='C:/Users/Dare/Desktop/crawler/simona_data/temp/'
csvFileName = 'vladimir.csv'
status = True



