import matplotlib as mpl

mpl.use('Agg')
import helpers as h
from search_config import SearchConfig
import pandas as pd
from tools import tools
import math
import networkx as nx
import numpy as np
import networkx_tools as nxt
from collections import OrderedDict
from slugify import slugify
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.patches as mpatch
from output_graph_metrics import plot_all_graphs


def plot_all(search: SearchConfig):
    search.set_status("Plotting", "Starting")
    plot_year(search.file_results_aggr_year, add_lin_coef=search.add_lin_coef, extension=search.plot_type,
              capitalize=search.capitalize)
    plot_source(search.file_results_aggr_source, extension=search.plot_type, capitalize=search.capitalize)
    plot_year_source(search.file_results_aggr_source_year_pivot_relevant,
                     add_lin_coef=search.add_lin_coef, extension=search.plot_type, capitalize=search.capitalize)
    plot_year_property_group(search.file_results_aggr_property_group_year_pivot,
                             add_lin_coef=search.add_lin_coef, extension=search.plot_type, capitalize=search.capitalize)
    plot_year_property(search.file_results_aggr_property_year_pivot,
                       add_lin_coef=search.add_lin_coef, extension=search.plot_type, capitalize=search.capitalize)
    plot_year_keyword(search.file_results_aggr_keyword_year_pivot_relevant,
                      add_lin_coef=search.add_lin_coef, extension=search.plot_type, capitalize=search.capitalize)
    plot_keyword_source(search.file_results_aggr_keyword_source_pivot_relevant, extension=search.plot_type,
                        capitalize=search.capitalize)

    create_property_graph(filename=search.file_results_graph_data,
                          filename_plot=search.file_results_graph_plot, extension=search.plot_type,
                          capitalize=search.capitalize)
    create_country_graph(filename=search.file_results_graph_data,
                         filename_plot=search.file_results_graph_plot, extension=search.plot_type)
    generate_prisma_output(search.file_results_aggr_source, search.file_results_aggr_prisma,
                           plot_type=search.plot_type)
    try:
        plot_all_graphs(search.file_results_articles_filtered, search.file_results_aggr_property_year_pivot,
                        num_intervals=4, plot_type=search.plot_type, capitalize=search.capitalize)
    except Exception as e:
        h.log_line('Ah toj Jane, ah toj Jane...', search.file_results_articles_filtered,
                   search.file_results_aggr_property_year_pivot, e)
    search.set_status("Plotting")


def generate_prisma_output(src_filename, dest_filename, plot_type='pdf'):
    # Prepare stats for the PRISMA chart
    h.csv_to_excel(src_filename)
    df = pd.read_csv(src_filename, sep='\t')
    df['incomplete_data'] = df['considered'] - df['invalid_years'] - df['duplicates'] - df['remaining']

    prisma_stats = OrderedDict()
    prisma_stats['identified_database'] = int(df['considered'].sum())
    prisma_stats['identified_other'] = 0
    prisma_stats['_duplicates'] = int(df['duplicates'].sum())
    prisma_stats['after_duplicates_removed'] = prisma_stats['identified_database'] + prisma_stats[
        'identified_other'] - prisma_stats['_duplicates']
    prisma_stats['records_screened'] = prisma_stats['after_duplicates_removed']
    prisma_stats['_invalid_years'] = int(df['invalid_years'].sum())
    prisma_stats['_incomplete_data'] = int(df['incomplete_data'].sum())
    prisma_stats['records_screened_excluded'] = prisma_stats['_invalid_years'] + prisma_stats['_incomplete_data']
    prisma_stats['assessed_eligibility'] = prisma_stats['records_screened'] - prisma_stats[
        'records_screened_excluded']
    prisma_stats['assessed_eligibility_excluded'] = 0
    prisma_stats['qualitative_synthesis'] = int(df['relevant'].sum())
    prisma_stats['assessed_eligibility_excluded'] = prisma_stats['assessed_eligibility'] - prisma_stats[
        'qualitative_synthesis']
    prisma_stats['quantitative_synthesis'] = -1
    df_prisma_stats = pd.DataFrame(list(prisma_stats.items()), columns=['metric', 'count'])
    df_prisma_stats.to_csv(dest_filename, index=False, sep='\t')
    h.csv_to_excel(dest_filename)
    plot_prisma(dest_filename, extension=plot_type)
    plot_prisma_scoping(dest_filename, extension=plot_type)


def generate_prisma_plots(search: SearchConfig, input_file: str = None):
    if input_file is None:
        input_file = search.file_results_aggr_prisma
    plot_prisma(input_file)
    plot_prisma_scoping(input_file)


def plot_prisma_scoping(src_filename, dest_filename=None, extension='pdf'):
    if not dest_filename:
        dest_filename = h.remove_extension(src_filename).replace('_pivot', '') + '_scoping.' + extension
    df = pd.read_csv(src_filename, sep='\t', header=0)
    d = dict(list(zip(df['metric'].tolist(), df['count'].tolist())))
    try:
        if int(d['quantitative_synthesis']) > 0:
            d['quantitative_synthesis'] = int(d['quantitative_synthesis'])
    except:
        d['quantitative_synthesis'] = 'n/a'
    # https://matplotlib.org/examples/color/named_colors.html
    # https://stackoverflow.com/questions/14531346/how-to-add-a-text-into-a-rectangle
    fig, ax = plt.subplots()
    left_rectangles = {
        'Identification': mpatch.Rectangle((0, 18.5), 1, 5, facecolor='skyblue', edgecolor='black', linewidth=0.5),
        'Screening': mpatch.Rectangle((2, 11.5), 1, 6, facecolor='lightskyblue', edgecolor='black', linewidth=0.5),
        'Eligibility': mpatch.Rectangle((2, 6), 1, 4, facecolor='lightskyblue', edgecolor='black', linewidth=0.5),
        'Selection': mpatch.Rectangle((0, 6), 1, 11.5, facecolor='skyblue', edgecolor='black', linewidth=0.5),
        'Included': mpatch.Rectangle((0, 1), 1, 4, facecolor='skyblue', edgecolor='black', linewidth=0.5)}

    for r_text, r in left_rectangles.items():
        ax.add_artist(r)
        rx, ry = r.get_xy()
        cx = rx + r.get_width() / 2.0
        cy = ry + r.get_height() / 2.0
        ax.annotate(r_text, (cx, cy), color='black', weight='bold',
                    fontsize=4, verticalalignment='center', horizontalalignment='center', rotation=90)
    rnames = {}
    rnames[1] = 'Records identified through\ndatabase searching\n(n=%s)' % h.fmt_number(d['identified_database'])
    rnames[2] = 'Additional records identified\nthrough other sources\n(n=%s)' % h.fmt_number(d['identified_other'])
    rnames[3] = 'Records after duplicates removed\n(n=%s)' % h.fmt_number(d['after_duplicates_removed'])
    rnames[4] = 'Records screened\n(n=%s)' % h.fmt_number(d['records_screened'])
    rnames[5] = 'Records excluded\n(n=%s)' % h.fmt_number(d['records_screened_excluded'])
    rnames[6] = "Articles' abstracts assessed\nfor eligibility\n(n=%s)" % h.fmt_number(d['assessed_eligibility'])
    rnames[7] = 'Articles excluded,\nwith reasons\n(n=%s)' % h.fmt_number(
        d['assessed_eligibility_excluded'])
    rnames[8] = 'Studies included in\ncollating and summarizing results\n(n=%s)' % h.fmt_number(
        d['qualitative_synthesis'])

    other_rectangles = {
        rnames[1]: mpatch.Rectangle((3, 19.5), 8, 3, facecolor='white', edgecolor='black', linewidth=0.5),
        rnames[2]: mpatch.Rectangle((13, 19.5), 8, 3, facecolor='white', edgecolor='black', linewidth=0.5),
        rnames[3]: mpatch.Rectangle((7, 15.5), 10, 2, facecolor='white', edgecolor='black', linewidth=0.5),
        rnames[4]: mpatch.Rectangle((8, 11.5), 8, 2, facecolor='white', edgecolor='black', linewidth=0.5),
        rnames[5]: mpatch.Rectangle((18, 11.5), 8, 2, facecolor='white', edgecolor='black', linewidth=0.5),
        rnames[6]: mpatch.Rectangle((8, 6.5), 8, 3, facecolor='white', edgecolor='black', linewidth=0.5),
        rnames[7]: mpatch.Rectangle((18, 6.5), 8, 3, facecolor='white', edgecolor='black', linewidth=0.5),
        rnames[8]: mpatch.Rectangle((8, 1.5), 8, 3, facecolor='white', edgecolor='black', linewidth=0.5),
    }

    for r_text, r in other_rectangles.items():
        ax.add_artist(r)
        rx, ry = r.get_xy()
        cx = rx + r.get_width() / 2.0
        cy = ry + r.get_height() / 2.0

        ax.annotate(r_text, (cx, cy), color='black',
                    fontsize=4, verticalalignment='center', horizontalalignment='center', rotation=0)

    plt.arrow(x=9, y=19.5, dx=0, dy=-1.75, head_width=0.1, linewidth=0.5)
    plt.arrow(x=15, y=19.5, dx=0, dy=-1.75, head_width=0.1, linewidth=0.5)
    plt.arrow(x=12, y=15.5, dx=0, dy=-1.75, head_width=0.1, linewidth=0.5)
    plt.arrow(x=12, y=11.5, dx=0, dy=-1.75, head_width=0.1, linewidth=0.5)
    plt.arrow(x=12, y=6.5, dx=0, dy=-1.75, head_width=0.1, linewidth=0.5)
    # plt.arrow(x=12, y=6, dx=0, dy=-1.75, head_width=0.1, linewidth=0.5)
    plt.arrow(x=16, y=12.5, dx=1.75, dy=0, head_width=0.1, linewidth=0.5)
    plt.arrow(x=16, y=8, dx=1.75, dy=0, head_width=0.1, linewidth=0.5)

    ax.set_xlim((-0.5, 27))
    ax.set_ylim((0.5, 24))
    ax.set_aspect('equal')

    fig.frameon = False
    ax.set_axis_off()
    dpi = 300 if extension == 'png' else None
    plt.savefig(dest_filename, bbox_inches='tight', dpi=dpi)
    if extension != 'pdf':
        plt.savefig(h.remove_extension(dest_filename) + '.pdf', bbox_inches='tight')


def plot_prisma(src_filename, dest_filename=None, extension='pdf'):
    if not dest_filename:
        dest_filename = h.remove_extension(src_filename).replace('_pivot', '') + '.' + extension
    df = pd.read_csv(src_filename, sep='\t', header=0)
    d = dict(list(zip(df['metric'].tolist(), df['count'].tolist())))
    try:
        if int(d['quantitative_synthesis']) > 0:
            d['quantitative_synthesis'] = int(d['quantitative_synthesis'])
    except:
        d['quantitative_synthesis'] = 'n/a'
    # https://matplotlib.org/examples/color/named_colors.html
    # https://stackoverflow.com/questions/14531346/how-to-add-a-text-into-a-rectangle
    fig, ax = plt.subplots()
    left_rectangles = {
        'Identification': mpatch.Rectangle((0, 22), 1, 6, facecolor='skyblue', edgecolor='black', linewidth=0.5),
        'Screening': mpatch.Rectangle((0, 15), 1, 6, facecolor='skyblue', edgecolor='black', linewidth=0.5),
        'Eligibility': mpatch.Rectangle((0, 8), 1, 6, facecolor='skyblue', edgecolor='black', linewidth=0.5),
        'Included': mpatch.Rectangle((0, 1), 1, 6, facecolor='skyblue', edgecolor='black', linewidth=0.5)}

    for r_text, r in left_rectangles.items():
        ax.add_artist(r)
        rx, ry = r.get_xy()
        cx = rx + r.get_width() / 2.0
        cy = ry + r.get_height() / 2.0
        ax.annotate(r_text, (cx, cy), color='black', weight='bold',
                    fontsize=4, verticalalignment='center', horizontalalignment='center', rotation=90)
    rnames = {}
    rnames[1] = 'Records identified through\ndatabase searching\n(n=%s)' % h.fmt_number(d['identified_database'])
    rnames[2] = 'Additional records identified\nthrough other sources\n(n=%s)' % h.fmt_number(d['identified_other'])
    rnames[3] = 'Records after duplicates removed\n(n=%s)' % h.fmt_number(d['after_duplicates_removed'])
    rnames[4] = 'Records screened\n(n=%s)' % h.fmt_number(d['records_screened'])
    rnames[5] = 'Records excluded\n(n=%s)' % h.fmt_number(d['records_screened_excluded'])
    # rnames[6]='Full-text articles assessed\nfor eligibility\n(n=%s)'%h.fmt_number(d['assessed_eligibility'])
    rnames[6] = "Articles' abstracts assessed\nfor eligibility\n(n=%s)" % h.fmt_number(d['assessed_eligibility'])
    rnames[7] = 'Articles excluded,\nwith reasons\n(n=%s)' % h.fmt_number(
        d['assessed_eligibility_excluded'])
    # rnames[7]='Full-text articles excluded,\nwith reasons\n(n=%s)'%d['assessed_eligibility_excluded']
    rnames[8] = 'Studies included in\nqualitative synthesis\n(n=%s)' % h.fmt_number(d['qualitative_synthesis'])
    rnames[9] = 'Studies included in\nquantitative synthesis\nmeta analysis\n(n=%s)' % h.fmt_number(
        d['quantitative_synthesis'])

    other_rectangles = {
        rnames[1]: mpatch.Rectangle((3, 24), 8, 3, facecolor='white', edgecolor='black', linewidth=0.5),
        rnames[2]: mpatch.Rectangle((13, 24), 8, 3, facecolor='white', edgecolor='black', linewidth=0.5),
        rnames[3]: mpatch.Rectangle((7, 20), 10, 2, facecolor='white', edgecolor='black', linewidth=0.5),
        rnames[4]: mpatch.Rectangle((8, 16), 8, 2, facecolor='white', edgecolor='black', linewidth=0.5),
        rnames[5]: mpatch.Rectangle((18, 16), 8, 2, facecolor='white', edgecolor='black', linewidth=0.5),
        rnames[6]: mpatch.Rectangle((8, 11), 8, 3, facecolor='white', edgecolor='black', linewidth=0.5),
        rnames[7]: mpatch.Rectangle((18, 11), 8, 3, facecolor='white', edgecolor='black', linewidth=0.5),
        rnames[8]: mpatch.Rectangle((8, 6), 8, 3, facecolor='white', edgecolor='black', linewidth=0.5),
        rnames[9]: mpatch.Rectangle((8, 1), 8, 3, facecolor='white', edgecolor='black', linewidth=0.5)
    }

    for r_text, r in other_rectangles.items():
        ax.add_artist(r)
        rx, ry = r.get_xy()
        cx = rx + r.get_width() / 2.0
        cy = ry + r.get_height() / 2.0

        ax.annotate(r_text, (cx, cy), color='black',
                    fontsize=4, verticalalignment='center', horizontalalignment='center', rotation=0)

    plt.arrow(x=9, y=24, dx=0, dy=-1.75, head_width=0.1, linewidth=0.5)
    plt.arrow(x=15, y=24, dx=0, dy=-1.75, head_width=0.1, linewidth=0.5)
    plt.arrow(x=12, y=20, dx=0, dy=-1.75, head_width=0.1, linewidth=0.5)
    plt.arrow(x=12, y=16, dx=0, dy=-1.75, head_width=0.1, linewidth=0.5)
    plt.arrow(x=12, y=11, dx=0, dy=-1.75, head_width=0.1, linewidth=0.5)
    plt.arrow(x=12, y=6, dx=0, dy=-1.75, head_width=0.1, linewidth=0.5)
    plt.arrow(x=16, y=17, dx=1.75, dy=0, head_width=0.1, linewidth=0.5)
    plt.arrow(x=16, y=12.5, dx=1.75, dy=0, head_width=0.1, linewidth=0.5)

    ax.set_xlim((-0.5, 27))
    ax.set_ylim((0.5, 28.5))
    ax.set_aspect('equal')

    # # Turn on the minor TICKS, which are required for the minor GRID
    # ax.minorticks_on()

    # # Customize the major grid
    # ax.grid(which='major', linestyle='-', linewidth='0.5', color='red')
    # Customize the minor grid
    # ax.grid(which='minor', linestyle=':', linewidth='0.5', color='black')
    # ax.axis('off')
    fig.frameon = False
    ax.set_axis_off()
    dpi = 300 if extension == 'png' else None
    plt.savefig(dest_filename, bbox_inches='tight', dpi=dpi)
    if extension != 'pdf':
        plt.savefig(h.remove_extension(dest_filename) + '.pdf', bbox_inches='tight')


def create_property_graph(filename: str = None, filename_plot: str = None,
                          extension='pdf', color_map='log', capitalize=False):
    try:
        articles = pd.read_csv(filename, sep='\t', index_col=0, header=0)
        G = nx.Graph()
        property_pairs = OrderedDict()
        properties = {}
        for index, row in articles.iterrows():
            vals = row['properties'].split(',')
            found_props = []
            for prop in vals:
                found_props.append(prop)
                properties.setdefault(prop, 0)
                properties[prop] += 1
            found_props.sort()
            for i in range(len(found_props) - 1):
                for j in range(i + 1, len(found_props)):
                    pair_key = (found_props[i], found_props[j])
                    property_pairs.setdefault(pair_key, 0)
                    property_pairs[pair_key] += 1

        edge_weights = list(property_pairs.values())
        if len(edge_weights) < 100:
            thr_percentile = 0
        else:
            thr_percentile = 75
        edge_weights_thres = int(np.percentile(edge_weights, thr_percentile))
        selected_properties = set()
        for (property1, property2), weight in property_pairs.items():
            if weight >= edge_weights_thres:
                selected_properties.add(property1)
                selected_properties.add(property2)
                G.add_edge(property1, property2, weight=weight)

        for prop, weight in properties.items():
            if prop in selected_properties:
                G.add_node(prop, weight=weight)

        nx.write_gml(G, filename_plot + '_properties.gml')
        title = 'Shown keywords:{0} Shown pairs: {1} Minimum co-occurrence in articles: {2} ({3}-th percentile)'.format(
            len(G.nodes()), len(G.edges), edge_weights_thres, thr_percentile)
        # nxt.plot_graph(G, dest_file=filename_plot + '_properties_circular.' + extension, labels=True, title=title,
        #                graph_type='circular', node_size_multiplier=1)
        nxt.plot_graph(G, dest_file=filename_plot + '_properties_spring.' + extension, labels=True, title=title,
                       graph_type='spring', node_size_multiplier=1, color_map=color_map)
        nx.write_gml(G, filename_plot + '_properties.gml')
        df1 = pd.DataFrame(list(properties.items()), columns=['property', 'weight'])
        df1.to_csv(filename_plot + '_property_nodes.csv', index=False, sep='\t')
        df2 = pd.DataFrame(list([(k[0], k[1], v) for k, v in property_pairs.items()]),
                           columns=['property1', 'property2', 'weight'])
        df2.to_csv(filename_plot + '_property_edges.csv', index=False, sep='\t')
    except Exception as e:
        h.log_line('Unable to plot graph.', e)


def create_country_graph(filename: str = None, filename_plot: str = None,
                         max_countries=50, extension='pdf', color_map='log'):
    try:
        articles = pd.read_csv(filename, sep='\t', index_col=0, header=0)
        G = nx.Graph()
        country_pairs = OrderedDict()
        countries = {}
        countries_collab = {}
        for index, row in articles.iterrows():
            vals = row['countries'].split(';')
            found_countriess = []
            for country in vals:
                found_countriess.append(country)
                countries.setdefault(country, 0)
                countries[country] += 1
                countries_collab.setdefault(country, 0)
            found_countriess.sort()
            for i in range(len(found_countriess) - 1):
                for j in range(i + 1, len(found_countriess)):
                    pair_key = (found_countriess[i], found_countriess[j])
                    country_pairs.setdefault(pair_key, 0)
                    country_pairs[pair_key] += 1
                    countries_collab[pair_key[0]] += 1
                    countries_collab[pair_key[1]] += 1
        top_countries = sorted(countries_collab.items(), key=lambda x: x[1], reverse=True)
        top_countries = set([k[0] for k in top_countries[:max_countries]])
        selected_pairs = dict([((u, v), w) for ((u, v), w) in country_pairs.items() if
                               u in top_countries and v in top_countries])
        # collab_perc = 85
        collab_perc = 90
        lst_values = list(selected_pairs.values())
        if len(lst_values) < 100:
            collab_perc = 0
        else:
            collab_perc = 90
        collab_threshold = int(np.percentile(lst_values, collab_perc))
        shown_countries = set()
        # collab_threshold = int(np.percentile(list(country_pairs.values()), 95))
        for (country1, country2), weight in selected_pairs.items():
            if weight >= collab_threshold:
                shown_countries.add(country1)
                shown_countries.add(country2)
                G.add_edge(country1, country2, weight=weight)
        for country in shown_countries:
            G.add_node(country, weight=countries[country])
        title = 'Shown countries:{0} Shown pairs: {1} Minimum collaborations: {2} ({3}-th percentile)'.format(
            len(G.nodes()), len(G.edges),
            collab_threshold, collab_perc)
        # nxt.plot_graph(G, dest_file=filename_plot + '_countries_circular.' + extension, labels=True,
        #                node_size_multiplier=0.5, graph_type='circular', title=title)
        nxt.plot_graph(G, dest_file=filename_plot + '_countries_spring.' + extension, labels=True,
                       node_size_multiplier=0.5, graph_type='spring', title=title, color_map=color_map)
        nx.write_gml(G, filename_plot + '_countries.gml')
        df1 = pd.DataFrame(list(countries.items()), columns=['property', 'weight'])
        df1.to_csv(filename_plot + '_country_nodes.csv', index=False, sep='\t')
        df2 = pd.DataFrame(list([(k[0], k[1], v) for k, v in country_pairs.items()]),
                           columns=['property1', 'property2', 'weight'])
        df2.to_csv(filename_plot + '_country_edges.csv', index=False, sep='\t')
    except Exception as e:
        h.log_line('Unable to plot graph.', e)


def plot_year(src_filename, dest_filename=None, add_lin_coef=True, extension='pdf', capitalize=False):
    try:
        df = pd.read_csv(src_filename, index_col='year', sep='\t')
        # df1.columns = [x.replace('_', ' ') for x in df1.columns]
        if add_lin_coef:
            tools.add_lin_coef_to_df_name(df)
        # https://stackoverflow.com/questions/25447700/annotate-bars-with-values-on-pandas-bar-plots
        # mpl.rcParams.update(mpl.rcParamsDefault)
        # print(mpl.rcParamsDefault['figure.figsize'])
        width = 6.4
        num_years = len(df.index)
        if num_years >= 20:
            width *= 2
        mpl.rcParams['figure.figsize'] = [width, 4.8]
        # mpl.style.use('tableau-colorblind10')
        # mpl.style.use('default')
        rotation = 0
        if num_years > 11:
            rotation = 45
        if capitalize:
            df.columns = [x.replace('_', ' ').capitalize() for x in df.columns]
        else:
            df.columns = [x.replace('_', ' ') for x in df.columns]
        ax = df.plot.bar(rot=rotation, fontsize=10, align='center')
        ax.get_yaxis().set_major_formatter(matplotlib.ticker.FuncFormatter(lambda x, p: h.fmt_number(x)))
        ax.yaxis.grid(linestyle='dotted')
        # Add labels to the plot
        # https://matplotlib.org/2.0.2/examples/color/named_colors.html
        _add_labels(ax, df)

        ax.set_ylabel('Number of articles')
        ax.set_xlabel('Year')
        _add_legend(ax, df, rotation=rotation)
        if dest_filename is None:
            dest_filename = h.remove_extension(src_filename).replace('_pivot', '') + '.' + extension
        dpi = 300 if extension == 'png' else None
        ax.get_figure().savefig(dest_filename, bbox_inches='tight', dpi=dpi)
        if extension != 'pdf':
            ax.get_figure().savefig(h.remove_extension(dest_filename) + '.pdf', bbox_inches='tight')
    except Exception as e:
        h.log_line('Error plotting:', src_filename, e)


def plot_source(src_filename, dest_filename=None, extension='pdf', capitalize=False):
    try:
        df = pd.read_csv(src_filename, index_col='source', sep='\t')
        # make sure numbers add up
        df = df[df['considered'] > 0]
        df['incomplete_data'] = df['considered'] - df['invalid_years'] - df['duplicates'] - df['remaining']
        if capitalize:
            df.columns = [x.replace('_', ' ').capitalize() for x in df.columns]
        else:
            df.columns = [x.replace('_', ' ') for x in df.columns]
        df.index = df.index.str.replace('NCBI:', '')
        df.sort_index(inplace=True)
        mpl.rcParams['figure.figsize'] = [12.8, 4.8]
        ax = df.plot.bar(rot=0, fontsize=10, align='center')
        ax.get_yaxis().set_major_formatter(matplotlib.ticker.FuncFormatter(lambda x, p: h.fmt_number(x)))
        ax.yaxis.grid(linestyle='dotted')
        _add_labels(ax, df)
        ax.set_ylabel('Number of articles')
        ax.set_xlabel('Digital library')
        _add_legend(ax, df)
        if dest_filename is None:
            dest_filename = h.remove_extension(src_filename).replace('_pivot', '') + '.' + extension
        dpi = 300 if extension == 'png' else None
        ax.get_figure().savefig(dest_filename, bbox_inches='tight', dpi=dpi)
        if extension != 'pdf':
            ax.get_figure().savefig(h.remove_extension(dest_filename) + '.pdf', bbox_inches='tight')
    except Exception as e:
        h.log_line('Error plotting:', src_filename, e)


def plot_year_property_group(src_filename, dest_filename=None, add_lin_coef=True, extension='pdf', capitalize=False):
    try:
        df = pd.read_csv(src_filename, sep='\t', index_col='property_group')
        df = df.T
        fig_width = 6.4
        horz_stretch = 1.0
        num_years = len(df.index)
        if len(df.columns) > 3 or num_years >= 20:
            horz_stretch = 2.0
        fig_width = int(fig_width * horz_stretch)

        rotation = 0

        if (num_years > 11 and horz_stretch < 2) or num_years >= 20:
            rotation = 45
        if add_lin_coef:
            tools.add_lin_coef_to_df_name(df)

        mpl.rcParams['figure.figsize'] = [fig_width, 4.8]
        if capitalize:
            df.columns = [x.replace('_', ' ').capitalize() for x in df.columns]
        else:
            df.columns = [x.replace('_', ' ') for x in df.columns]
        ax = df.plot.bar(rot=rotation, align='center', fontsize=10)
        ax.get_yaxis().set_major_formatter(matplotlib.ticker.FuncFormatter(lambda x, p: h.fmt_number(x)))
        ax.yaxis.grid(linestyle='dotted')
        ax.set_ylabel('Number of articles')
        ax.set_xlabel('Year')
        _add_labels(ax, df)
        _add_legend(ax, df, rotation=rotation)
        if dest_filename is None:
            dest_filename = h.remove_extension(src_filename).replace('_pivot', '') + '.' + extension
        dpi = 300 if extension == 'png' else None
        ax.get_figure().savefig(dest_filename, bbox_inches='tight', dpi=dpi)
        if extension != 'pdf':
            ax.get_figure().savefig(h.remove_extension(dest_filename) + '.pdf', bbox_inches='tight')
    except Exception as e:
        h.log_line('Error plotting:', src_filename, e)


def plot_year_source(src_filename, dest_filename=None, add_lin_coef=True, extension='pdf', capitalize=False):
    try:
        df = pd.read_csv(src_filename, sep='\t', index_col='source')
        df.index = df.index.str.replace('NCBI:', '')
        df = df.T
        df = df.reindex(sorted(df.columns), axis=1)
        df.sort_index(inplace=True)
        if add_lin_coef:
            tools.add_lin_coef_to_df_name(df)
        fig_width = 6.4
        horz_stretch = 1.0
        num_years = len(df.index)
        if len(df.columns) > 3 or num_years >= 20:
            horz_stretch = 2.0
        fig_width = int(fig_width * horz_stretch)

        rotation = 0
        if (num_years > 11 and horz_stretch < 2) or (horz_stretch >= 2 and num_years >= 20):
            rotation = 45
        mpl.rcParams['figure.figsize'] = [fig_width, 4.8]
        ax = df.plot.bar(rot=rotation, align='center', fontsize=10)
        ax.get_yaxis().set_major_formatter(matplotlib.ticker.FuncFormatter(lambda x, p: h.fmt_number(x)))
        ax.yaxis.grid(linestyle='dotted')
        ax.set_ylabel('Number of relevant articles')
        ax.set_xlabel('Year')
        _add_labels(ax, df)
        _add_legend(ax, df, rotation=rotation)
        if dest_filename is None:
            dest_filename = h.remove_extension(src_filename).replace('_pivot', '') + '.' + extension
        dpi = 300 if extension == 'png' else None
        ax.get_figure().savefig(dest_filename, bbox_inches='tight', dpi=dpi)
        if extension != 'pdf':
            ax.get_figure().savefig(h.remove_extension(dest_filename) + '.pdf', bbox_inches='tight')
    except Exception as e:
        h.log_line('Error plotting:', src_filename, e)


def plot_keyword_source(src_filename, dest_filename=None, extension='pdf', capitalize=False):
    try:
        df = pd.read_csv(src_filename, sep='\t', index_col='keyword')
        df.columns = [c.replace('NCBI:', '') for c in df.columns]
        df = df.T
        df = df.reindex(sorted(df.columns), axis=1)
        df.sort_index(inplace=True)
        fig_width = 6.4
        horz_stretch = 1.0
        if len(df.columns) > 3:
            horz_stretch = 2.0
        fig_width = int(fig_width * horz_stretch)

        mpl.rcParams['figure.figsize'] = [fig_width, 4.8]
        ax = df.plot.bar(rot=0, align='center', fontsize=10)
        ax.get_yaxis().set_major_formatter(matplotlib.ticker.FuncFormatter(lambda x, p: h.fmt_number(x)))
        ax.yaxis.grid(linestyle='dotted')
        ax.set_ylabel('Number of relevant articles')
        ax.set_xlabel('Source')
        _add_labels(ax, df)
        _add_legend(ax, df)
        if dest_filename is None:
            dest_filename = h.remove_extension(src_filename).replace('_pivot', '') + '.' + extension
        dpi = 300 if extension == 'png' else None
        ax.get_figure().savefig(dest_filename, bbox_inches='tight', dpi=dpi)
        if extension != 'pdf':
            ax.get_figure().savefig(h.remove_extension(dest_filename) + '.pdf', bbox_inches='tight')
    except Exception as e:
        h.log_line('Error plotting:', src_filename, e)


def plot_year_keyword(src_filename, dest_filename=None, add_lin_coef=True, extension='pdf', capitalize=False):
    try:
        df = pd.read_csv(src_filename, sep='\t', index_col='keyword')
        df = df.T
        df = df.reindex(sorted(df.columns), axis=1)
        df.sort_index(inplace=True)
        if add_lin_coef:
            tools.add_lin_coef_to_df_name(df)
        num_years = len(df.index)
        rotation = 0
        fig_width = 6.4
        horz_stretch = 1.0
        if len(df.columns) > 3 or num_years >= 20:
            horz_stretch = 2.0
        fig_width = int(fig_width * horz_stretch)

        if (num_years > 11 and horz_stretch < 2) or (horz_stretch >= 2 and num_years >= 20):
            rotation = 45

        mpl.rcParams['figure.figsize'] = [fig_width, 4.8]
        ax = df.plot.bar(rot=rotation, align='center', fontsize=10)
        ax.get_yaxis().set_major_formatter(matplotlib.ticker.FuncFormatter(lambda x, p: h.fmt_number(x)))
        ax.yaxis.grid(linestyle='dotted')
        ax.set_ylabel('Number of relevant articles')
        ax.set_xlabel('Year')
        _add_labels(ax, df)
        _add_legend(ax, df, rotation)
        if dest_filename is None:
            dest_filename = h.remove_extension(src_filename).replace('_pivot', '') + '.' + extension
        dpi = 300 if extension == 'png' else None
        ax.get_figure().savefig(dest_filename, bbox_inches='tight', dpi=dpi)
        if extension != 'pdf':
            ax.get_figure().savefig(h.remove_extension(dest_filename) + '.pdf', bbox_inches='tight')
    except Exception as e:
        h.log_line('Error plotting:', src_filename, e)


def plot_year_property(src_filename: str, dest_filename: str = None, add_lin_coef=True, extension='pdf',
                       capitalize=False):
    """
    Generates a separate chart for each property.
    :param src_filename:
    :param dest_filename: If None, it will be generated from ```src_filename```.
    :return:
    """
    try:
        df = pd.read_csv(src_filename, sep='\t', index_col=None)
        property_groups = sorted(set(df['property_group'].values))
        if dest_filename:
            dest_filename = h.remove_extension(dest_filename).replace('_pivot', '')
        else:
            dest_filename = h.remove_extension(src_filename).replace('_pivot', '')
        dpi = 300 if extension == 'png' else None
        for prop_gr in property_groups:
            cdf = df[df['property_group'] == prop_gr]
            cdf = cdf.drop('property_group', axis=1)
            cdf.set_index('property', inplace=True)
            cdf = cdf.T
            if add_lin_coef:
                tools.add_lin_coef_to_df_name(cdf)
            fig_width = 6.4
            horz_stretch = 1.0
            num_years = len(cdf.index)
            rotation = 0
            if len(cdf.columns) > 3 or num_years >= 20:
                horz_stretch = 2.0
            if (num_years > 11 and horz_stretch < 2) or (horz_stretch >= 2 and num_years >= 20):
                rotation = 45
            fig_width = int(fig_width * horz_stretch)

            mpl.rcParams['figure.figsize'] = [fig_width, 4.8]
            if capitalize:
                cdf.columns = [x.replace('_', ' ').capitalize() for x in cdf.columns]
            else:
                cdf.columns = [x.replace('_', ' ') for x in cdf.columns]
            ax = cdf.plot.bar(rot=rotation, align='center', fontsize=10)
            ax.get_yaxis().set_major_formatter(matplotlib.ticker.FuncFormatter(lambda x, p: h.fmt_number(x)))
            ax.yaxis.grid(linestyle='dotted')
            ax.set_ylabel('Number of articles')
            ax.set_xlabel('Year')
            # ax.set_title(prop_gr)
            _add_labels(ax, cdf)
            _add_legend(ax, cdf, rotation=rotation)
            cdest_filename = dest_filename.replace('_pivot', '') + '_' + slugify(prop_gr) + '.' + extension
            h.log_line('plotting to', cdest_filename)
            ax.get_figure().savefig(cdest_filename, bbox_inches='tight', dpi=dpi)
            if extension != 'pdf':
                ax.get_figure().savefig(h.remove_extension(cdest_filename) + '.pdf', bbox_inches='tight')
    except Exception as e:
        h.log_line('Error plotting:', src_filename, e)


def _add_labels(ax, df: pd.DataFrame, rotation=90):
    """
    Adds labels to each rectangle in a bar plot
    :param ax: matplotlib.axes.Axes object
    :param df:
    :return:
    """
    size_dict = {1: 8, 2: 8, 3: 7, 4: 7, 5: 7, 6: 6, 7: 6, 8: 5}
    size = size_dict.get(len(df.columns), 4.5)
    style = dict(size=size, color='black', rotation=rotation)
    x_offset = ax.patches[0].get_width() / 2.0
    # max_height = for p in ax.patches
    for i, p in enumerate(ax.patches):
        if rotation == 90:
            ax.annotate(h.fmt_number(p.get_height()), (p.get_x() + x_offset, p.get_height()),
                        verticalalignment='bottom', horizontalalignment='center', **style)
        else:
            ax.annotate(h.fmt_number(p.get_height()), (p.get_x() + x_offset, p.get_height() * 1.015), ha='center',
                        **style)


def _add_legend(ax, df: pd.DataFrame, rotation=0):
    """
    Adds legend below the bar chart
    :param ax:
    :param df:
    :param 0:
    :return:
    """
    legend_max_chars = 7 * 10 + 64  # 7 labels*10 + 64 chars
    # legend_max_chars = min(64, int(46 * horz_stretch))  # empirically 46 chars for plot with width=6.4 and font=10
    col_lens = [len(cn) for cn in df.columns]
    legend_len = sum(col_lens) + 10 * len(df.columns)
    legend_cols = len(df.columns)
    while legend_cols > 1 and legend_len > legend_max_chars:
        legend_cols = int(min(math.ceil(legend_cols / 2.0), legend_cols - 1))
        legend_len = 0
        for bucket in range(legend_cols):
            bucket_cols = col_lens[bucket * legend_cols:(bucket + 1) * legend_cols]
            bucket_len = sum(bucket_cols) + 10 * len(bucket_cols)
            legend_len = max(legend_len, bucket_len)
    # ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.12), ncol=len(y.columns), fancybox=True, shadow=True)
    if rotation == 0:
        ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.12), ncol=legend_cols, frameon=False, shadow=True)
    elif rotation == 45:
        ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.16), ncol=legend_cols, frameon=False, shadow=True)
    else:
        ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.2), ncol=legend_cols, frameon=False, shadow=True)


def generate_filtered_graph_data_csv(fn_filtered_articles, fn_graph_data, relevant=3):
    try:
        data, header = h.read_csv_file(fn_filtered_articles, separator='\t', datatype='str', separate_header=True)
        graph_data = [['doi', 'affiliations', 'countries', 'properties']]
        for row in data:
            try:
                doi = row[0]
                affiliations = row[11]
                countries = row[13]
                num_properties = int(row[18])
            except:
                continue
            s_properties = ','.join([v for v in row[19:] if len(v) > 0])
            # l_properties = sorted(set(s_properties.split(',')))
            if num_properties < max(2, relevant):
                continue
            countries = countries.replace('Netherlands', 'The Netherlands')
            graph_data.append([doi, affiliations, countries, s_properties])
        h.write_csv_file(graph_data, fn_graph_data, separator='\t')
    except Exception as e:
        h.log_line('Error plotting:', fn_filtered_articles, e)
