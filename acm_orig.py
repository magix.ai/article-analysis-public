from browser_emulate import *
import helpers as h
import csv
from selenium.webdriver.common.keys import Keys
from time import sleep
from selenium.common.exceptions import NoSuchElementException
from tools import tools  
from lxml import html  
from urllib.parse import urlparse
import pathlib


def download_file(url, download_dir, filename=None, return_string=False):
	if filename is None:
		filename = tools.html_filename(url)
	final_url = url
	if os.path.isfile(filename) and os.stat(filename).st_size > 0:
		h.log_line('File exists:', filename, 'Skipping download of', url)
		with open(filename, 'r', encoding="utf-8") as file:
			s = file.read()
	else:
		browser = create_browser(download_dir)
		browser.get(url)
		sleep(3)
		s = browser.page_source
		with open(filename,'w+', encoding="utf-8") as f:
			f.write(s)
		browser.close()
	if return_string:
		web_page = s 
	else:
		web_page = html.fromstring(s)
	return web_page, final_url



url='https://dl.acm.org/results.cfm?query=(vladimir%20trajkovik)&within=owners.owner=HOSTED&filtered=&dte=2017&bfr=2018'
download_dir='C:/Users/Dare/Desktop/crawler/simona_data/temp/'
csvFileName = 'vladimir.csv'
status = True

_doi_xpaths = ['//*[@id="divmain"]/table[2]/tbody/tr/td/table/tbody/tr[5]/td/span[5]/a',
					'//*[@id="divmain"]/table[2]/tbody/tr/td/table/tbody/tr[5]/td/span[4]/a',
					'//*[@id="divmain"]/table[2]/tbody/tr/td/table/tbody/tr[5]/td/span[3]/a',
					'//*[@id="divmain"]/table[2]/tbody/tr/td/table/tbody/tr[4]/td/span[4]/a',
					'//*[@id="divmain"]/table[2]/tbody/tr/td/table/tbody/tr[5]/td/span/a',
					'//*[@id="divmain"]/table/tbody/tr/td[1]/table[3]/tbody/tr/td/table/tbody/tr[5]/td/span[4]/a',
					'//*[@id="divmain"]/table/tbody/tr/td[1]/table[3]/tbody/tr/td/table/tbody/tr[4]/td/span[5]/a',
					'//*[@id="divmain"]/table/tbody/tr/td[1]/table[3]/tbody/tr/td/table/tbody/tr[4]/td/span[4]/a'
					]

_authors_affiliations_xpaths = ['//*[@id="divmain"]/table/tbody/tr/td[1]/table[2]/tbody',
								'//*[@id="divmain"]/table[1]/tbody/tr/td[1]/table[2]/tbody',
								]




parsed_uri = urlparse(url)
base_url = '{uri.scheme}://{uri.netloc}/'.format(uri=parsed_uri)
local_base_url = pathlib.Path(download_dir).as_uri() + '/'

browser = create_browser(download_dir)

filename = tools.html_filename(url, download_dir)
web_page, final_url = tools.download_page(url, filename)

browser.get(pathlib.Path(filename).as_uri())
wait = WebDriverWait(browser, Config.browser_delay_time_acm)

current_page = 1
header = ('title', 'abstract', 'doi', 'publicationTitle', 'publicationDate', 'publicationYear', 'authors', 'downloadCount', 'citedCount', 'authors2', 'authors_affilations', 'link')
detailsList = []
detailsList.append(header)

while True:
	wait.until(EC.presence_of_element_located((By.CLASS_NAME,'details')))
	elements = browser.find_elements_by_class_name('details')

	for element in elements:
		title = element.find_element_by_class_name('title')
		print('\ntitle: ', title.text)

		try:
			authors = element.find_element_by_class_name('authors').text
		except NoSuchElementException as e:
			authors = ""

		source = None
		publicationDate = None
		try:
			source = element.find_element_by_class_name('source')

			publicationDate = source.find_element_by_class_name('publicationDate')
			publicationYear = None
			if publicationDate: 
				publicationYear = [int(s) for s in publicationDate.text.split() if s.isdigit()]
				publicationYear = publicationYear[0]

			publicationDate = publicationDate.text
		except NoSuchElementException as e:
			h.log_line('Can\'t find publication date for:', title, 'search:', url_search, e)

		try:
			publicationTitle = source.find_element_by_xpath('span[2]').text
		except NoSuchElementException as e:
			publicationTitle = None

		try:
			citedCount = element.find_element_by_class_name('citedCount')
			if citedCount:
				citedCount = [int(s.replace(',','')) for s in citedCount.text.split() if s.replace(',','').isdigit()]
				if len(citedCount) > 0:
					citedCount = citedCount[0]
				else:
					citedCount = None
		except NoSuchElementException as e:
			citedCount = None

		try:
			downloadCount = element.find_element_by_class_name('downloadAll')
			if downloadCount:
				downloadCount = [int(s.replace(',','')) for s in downloadCount.text.split() if s.replace(',','').isdigit()]
				if len(downloadCount) > 0:
					downloadCount = downloadCount[0]
				else:
					downloadCount = None
		except NoSuchElementException as e:
			downloadCount = None

		main_window = browser.current_window_handle

		link = title.find_element_by_tag_name('a').get_attribute('href')
		element_url = link.replace(local_base_url, base_url)
		filename = tools.html_filename(element_url, download_dir)
		web_page, final_url = download_file(element_url, download_dir, filename)

		browser.execute_script("window.open('');")
		sleep(3)
		browser.switch_to.window(browser.window_handles[1])
		browser.get(pathlib.Path(filename).as_uri())

		wait.until(EC.presence_of_element_located((By.XPATH,'//*[@id="cf_layoutareaabstract"]')))
		try:
			abstract = browser.find_element_by_xpath('//*[@id="cf_layoutareaabstract"]/div/div/p').text
		except NoSuchElementException as e:
			abstract = 'An abstract is not available.'

		doi = None
		for xp in _doi_xpaths:
			try:
				doi = browser.find_element_by_xpath(xp).text
			except NoSuchElementException as e:
				doi = None
				continue
			if doi: break

	
		try:
			authors_str = None
			affilations_str = None
			authors_affilations_wrapper = browser.find_elements_by_xpath('//*[@id="divmain"]/table[1]/tbody/tr/td[1]/table[2]/tbody/tr')
			if authors_affilations_wrapper:
				author_list = []
				affilation_list = []
				for author_affilation in authors_affilations_wrapper:
					author = author_affilation.find_element_by_tag_name('a')
					affilation = author_affilation.find_element_by_tag_name('small')
					author_list.append(author.text)
					affilation_list.append(affilation.text)
				authors_str = ';'.join(author_list)
				affilations_str = ';'.join(affilation_list)
		except Exception as e:
			authors_str = None
			affilations_str = None
			print(e)


		browser.close()
		sleep(2)
		browser.switch_to.window(main_window)
		
		row = (title.text,abstract,doi,publicationTitle,publicationDate,publicationYear,authors,downloadCount,citedCount,authors_str,affilations_str,element_url)
		detailsList.append(row)
		# print(row)
		

	pages = browser.find_elements_by_xpath('//*[@id="results"]/div[2]/span/a')	
	found = None
	for page in pages:
		if int(page.text) > current_page:
			# print(page.text)
			current_page = int(page.text)
			found = True
			next_page_url = page.get_attribute('href').replace(local_base_url, base_url)
			filename = tools.html_filename(next_page_url, download_dir)
			web_page, final_url = tools.download_page(next_page_url, filename)
			browser.get(pathlib.Path(filename).as_uri())
			break
	if found == None:
		print("done with pages")
		break

browser.close()

file = download_dir + csvFileName
csv.register_dialect('myDialect', lineterminator = '\n')
with open(file, 'w+', encoding="utf-8") as csvFile:
	writer = csv.writer(csvFile, dialect='myDialect')
	writer.writerows(detailsList)

