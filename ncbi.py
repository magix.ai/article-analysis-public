import os
import urllib
import xml.etree.ElementTree as ET

from slugify import slugify

import helpers as h
from browser_emulate import download_ncbi_files
from config import Config
from countries import Countries
from search_config import SearchConfig
from tools import tools


class ncbi(object):
    url_template = 'https://www.ncbi.nlm.nih.gov/{0}/?term={1}&cmd=correctspelling'
    libraries = {"PubMed": "pubmed",
                 "Protein": "protein",
                 "Nucleotide": "nuccore",
                 "Identical Protein Groups": "ipg",
                 "GSS": "nucgss",
                 "EST": "nucest",
                 "Structure": "structure",
                 "Sparcle": "sparcle",
                 "Genome": "genome",
                 "Assembly": "assembly",
                 "BioProject": "bioproject",
                 "BioSample": "biosample",
                 "Books": "books",
                 "Conserved Domains": "cdd",
                 "ClinVar": "clinvar",
                 "Clone": "clone",
                 "dbGaP": "gap",
                 "dbVar": "dbvar",
                 "Gene": "gene",
                 "GEO DataSets": "gds",
                 "GEO Profiles": "geoprofiles",
                 "HomoloGene": "homologene",
                 "MedGen": "medgen",
                 "MeSH": "mesh",
                 "NCBI Web Site": "ncbisearch",
                 "NLM Catalog": "nlmcatalog",
                 "OMIM": "omim",
                 "PMC": "pmc",
                 "PopSet": "popset",
                 "Probe": "probe",
                 "Protein Clusters": "proteinclusters",
                 "PubChem BioAssay": "pcassay",
                 "BioSystems": "biosystems",
                 "PubChem Compound": "pccompound",
                 "PubChem Substance": "pcsubstance",
                 "PubMed Health": "pubmedhealth",
                 "SNP": "snp",
                 "SRA": "sra",
                 "Taxonomy": "taxonomy",
                 "Biocollections": "biocollections",
                 "ToolKitAll": "toolkitall",
                 "ToolKit": "toolkit",
                 "ToolKitBookgh": "toolkitbookgh",
                 "UniGene": "unigene",
                 "GTR": "gtr"}
    libraries_reverse = dict([(v, k) for k, v in libraries.items()])
    _libraries_all = set([v.lower() for v in set(libraries.keys()).union(set(libraries.values()))])

    @staticmethod
    def crawl(search: SearchConfig, keyword: str = None, library='PubMed'):
        """

        :param search:
        :param keyword:
        :param library: One of: PubMed, PMC, PubMedHealth, etc... see `ncbi.libraries`
        :return:
        """
        search.set_status(library, 'Starting')
        lkey = ncbi.libraries.get(library, library.lower())
        ncbi.source = Config.sources['ncbi'] + ':' + ncbi.libraries_reverse[lkey]
        if lkey in ncbi._libraries_all:
            h.create_folder(os.path.join(search.folder_NCBI, lkey))
            h.log_line('Crawling NCBI:', library, ', coded as:', lkey)
            if keyword:
                ncbi._process_keyword(search, keyword, lkey)
            else:
                for keyword in search.keywords:
                    ncbi._process_keyword(search, keyword, lkey)
        search.set_status(library)
        search.to_pickle()

    @staticmethod
    def _process_keyword(search: SearchConfig, keyword: str, library_key: str):
        status = ncbi._crawl_keyword(os.path.join(search.folder_NCBI, library_key), keyword, library_key)
        if status:
            ncbi._clean_result(search, keyword, library_key)

    @staticmethod
    def _clean_result(search: SearchConfig, keyword=None, library_key: str = 'pubmed'):
        h.log_line('Cleaning NCBI results:', library_key)
        if keyword:
            ncbi._clean_result_keyword(search, keyword, library_key)
        else:
            for keyword in search.keywords:
                ncbi._clean_result_keyword(search, keyword, library_key)

    @staticmethod
    def _clean_result_keyword(search: SearchConfig, keyword, library_key):
        search.set_result_counts(ncbi.source)
        src_filename = os.path.join(search.folder_NCBI, library_key, slugify(keyword) + '.xml')
        dst_filename = os.path.join(search.folder_NCBI, library_key, slugify(keyword) + '_links.csv')
        # if os.path.isfile(src_filename) and os.stat(src_filename).st_size > 100 * 1000**3:
        if False:
            # TODO: test this and fix the bug
            # Slower, but lightweight
            data = ncbi._clean_result_file_incremental(src_filename, search, keyword)
        else:
            # Faster but memory intensive. E.x. 100 MB => 500 MB in memory
            data = ncbi._clean_result_file_in_memory(src_filename, search, keyword)
        search.write_results(data, dst_filename)

    @staticmethod
    def _clean_result_file_incremental(src_filename: str, search: SearchConfig = None, keyword: str = None):
        data = []
        if search is None:
            search = SearchConfig()
        h.log_line('Starting with file:', src_filename)
        try:
            # http://effbot.org/zone/element-iterparse.htm
            # get an iterable
            context = ET.iterparse(src_filename, events=("start", "end"))

            # turn it into an iterator
            context = iter(context)
            # get the root element
            event, root = next(context)
            data = []
            i = 0
            for event, elem in context:
                #     print(event, elem.tag)
                if event == "start" and elem.tag == "PubmedArticle":
                    i += 1
                    article_title, abstract, authors, pub_title = '', '', '', ''
                    num_authors = 0
                    pmid, doi = '', ''
                    s_affiliations = None
                    l_affiliations = []
                    year, pub_month, pub_day = None, None, None
                    citations = '-1'
                    for event, elem in context:
                        #     # print(num_elements, elem.tag, elem.text)
                        if event == "end" and elem.tag == "PubmedArticle":
                            break
                        if event != "start":
                            continue
                        elif elem.tag == 'ArticleTitle':
                            article_title = elem.text
                            article_title = h.remove_tabs(article_title).strip()[:3000]
                        elif elem.tag == 'Abstract':
                            abstract = elem.text
                            abstract = h.remove_nonalphanumeric(abstract).strip()[:3000]
                        elif elem.tag == 'Title':
                            pub_title = elem.text
                            pub_title = h.remove_nonalphanumeric(pub_title).strip()[:3000]
                        elif elem.tag == 'AuthorList':
                            last_names = []
                            first_names = []
                            for event_in, elem in context:
                                if event_in == "end" and elem.tag == "AuthorList":
                                    break
                                if elem.tag == 'LastName':
                                    last_names.append(elem.text)
                                elif elem.tag == 'ForeName':
                                    first_names.append(elem.text)
                            try:
                                authorsl = [(fn + ' ' + ln).replace(',', ' ') for fn, ln in
                                            zip(first_names, last_names)]
                                authors = ', '.join(authorsl)
                            except:
                                authors = ''
                            authors = authors.replace(';', ',')
                            num_authors = authors.count(',') + 1
                            authors = h.remove_tabs(authors).strip()[:3000]
                        elif not year and elem.tag in ('ArticleDate'):
                            for event_in, elem in context:
                                if event_in == "end" and elem.tag in ('ArticleDate'):
                                    break
                                if elem.tag == 'Year':
                                    year = elem.text
                                elif elem.tag == 'Month':
                                    pub_month = elem.text
                                elif elem.tag == 'Day':
                                    pub_day = elem.text
                        elif not year and elem.tag in ('PubMedPubDate'):
                            for event_in, elem in context:
                                if event_in == "end" and elem.tag in ('PubMedPubDate'):
                                    break
                                if elem.tag == 'Year' and elem.attrib.get('PubStatus', '') == 'accepted':
                                    year = elem.text
                                elif elem.tag == 'Month' and elem.attrib.get('PubStatus', '') == 'accepted':
                                    pub_month = elem.text
                                elif elem.tag == 'Day' and elem.attrib.get('PubStatus', '') == 'accepted':
                                    pub_day = elem.text
                        elif elem.tag == 'Affiliation':
                            l_affiliations.append(elem.text)
                        elif elem.tag == 'ArticleId' and elem.attrib.get('IdType', '') == 'pubmed':
                            pmid = elem.text
                        elif elem.tag == 'ArticleId' and elem.attrib.get('IdType', '') == 'doi':
                            doi = elem.text
                    link = 'https://www.ncbi.nlm.nih.gov/pubmed/' + pmid
                    if ('text' in doi or len(doi) == 0) and len(pmid) > 0:
                        doi = 'PubMed_' + pmid
                        link = 'https://www.ncbi.nlm.nih.gov/pubmed/' + pmid
                    if len(link) == 0 and len(doi) > 0:
                        link = 'https://doi.org/' + doi
                    if h.is_empty(doi):
                        _doi = tools.simplify_url(link)
                        if len(_doi) > 0:
                            doi = _doi
                        else:
                            doi = h.remove_nonalphanumeric(article_title, '').replace(' ', '_').lower()

                    date = year + '/' + pub_month + '/' + pub_day
                    year = year[:4]
                    if len(l_affiliations) > 0:
                        s_affiliations = ';'.join(l_affiliations)
                    affiliations, num_diff_affiliations, countries, num_diff_countries = Countries.process_affiliations(
                        s_affiliations)
                    if num_diff_countries > num_authors:
                        num_diff_countries = num_authors
                    try:
                        if len(year) == 4 and not (search.start_year <= int(year) <= search.end_year):
                            search.result_counts[ncbi.source]['invalid_years'] += 1
                            continue
                    except:
                        search.result_counts[ncbi.source]['invalid_years'] += 1
                        continue
                    if search.exists_doi(doi, ncbi.source):
                        search.result_counts[ncbi.source]['duplicates'] += 1
                        continue
                    r = (
                        doi, link, article_title, authors, date, year, citations, abstract, keyword, ncbi.source,
                        pub_title,
                        affiliations, num_diff_affiliations, countries, num_diff_countries, num_authors)
                    row = [h.remove_tabs(s) for s in r]

                    if len(row[7]) < Config.abstract_min_length:
                        search.result_counts[ncbi.source]['incomplete_data'] += 1
                        continue
                    if h.is_empty(authors):
                        search.result_counts[ncbi.source]['incomplete_data'] += 1
                        continue

                    data.append(row)
                    root.clear()
                    if i % 100 == 0:
                        h.log_line('Line', i, 'in file:', src_filename)
        except Exception as e:
            h.log_line('Error with', src_filename, e)
        h.log_line('Completed with file:', src_filename)
        return data

    @staticmethod
    def _clean_result_file_in_memory(src_filename: str, search: SearchConfig = None, keyword: str = None):
        data = []
        if search is None:
            search = SearchConfig()
        h.log_line('Starting with file:', src_filename)
        try:
            d = h.xml2dict(src_filename)
            for pa in d['PubmedArticleSet'].values():
                for i, a in enumerate(pa):
                    search.result_counts[ncbi.source]['considered'] += 1
                    article = a.get('MedlineCitation', {}).get('Article', {})
                    try:
                        title = article.get('ArticleTitle', {}).get('#text', '')
                    except:
                        try:
                            title = article.get('ArticleTitle', '')
                        except:
                            if 'str' in str(type(title)):
                                title = ''
                            else:
                                h.log_line(type(title), title)
                    title = h.remove_tabs(title).strip()[:3000]
                    try:
                        abstract = article.get('Abstract', {}).get('AbstractText', {}).get('#text', '')
                    except:
                        try:
                            abstract = article.get('Abstract', {}).get('AbstractText', '')
                            if 'list' in str(type(abstract)):
                                abstractl = [al.get('#text', '') for al in abstract]
                                abstract = ' '.join(abstractl)
                        except:
                            try:
                                abstract = str(article.get('Abstract', ''))
                            except:
                                abstract = ''

                    abstract = h.remove_nonalphanumeric(abstract).strip()[:3000]
                    try:
                        publication_title = article.get('Journal', {}).get('Title', {})
                    except Exception as expub:
                        publication_title = None
                    try:
                        authorsl = [(aut.get('LastName', '') + ' ' + aut.get('ForeName', '')).replace(',', ' ') for aut
                                    in article.get('AuthorList', {}).get('Author', {})]
                        authors = ', '.join(authorsl)
                    except:
                        try:
                            aut = article.get('AuthorList', {}).get('Author', {})
                            authors = aut.get('LastName', '') + ' ' + aut.get('ForeName', '')
                        except:
                            authors = str(article.get('AuthorList', {}).get('Author', ''))
                            authors = authors.replace(',', '')  # this shouldn't happen, so just clean up
                    authors = authors.replace(';', ',')
                    num_authors = authors.count(',') + 1
                    authors = h.remove_tabs(authors).strip()[:3000]
                    citations = '-1'
                    year = article.get('ArticleDate', {}).get('Year', '').strip()
                    pub_month = article.get('ArticleDate', {}).get('Month', '').strip()
                    pub_day = article.get('ArticleDate', {}).get('Day', '').strip()
                    s_affiliations = None
                    l_affiliations = []
                    try:
                        try:
                            for aut in article.get('AuthorList', {}).get('Author', {}):
                                affil = aut.get('AffiliationInfo', {}).get('Affiliation', None)
                                if affil:
                                    l_affiliations.append(affil)
                        except Exception as exafil:
                            s_affiliations = str(
                                article.get('AuthorList', {}).get('Author', {}).get('AffiliationInfo', ''))
                    except Exception as expub:
                        s_affiliations = None
                    if len(l_affiliations) > 0:
                        s_affiliations = ';'.join(l_affiliations)
                    affiliations, num_diff_affiliations, countries, num_diff_countries = Countries.process_affiliations(
                        s_affiliations)
                    if year == '':
                        try:
                            tsi = a.get('PubmedData', {}).get('History', {}).get('PubMedPubDate', {})
                            if 'list' in str(type(tsi)):
                                for ts in tsi:
                                    if ts.get('@PubStatus', '') == 'accepted':
                                        break
                            else:
                                ts = tsi
                        except:
                            ts = {}
                        year = ts.get('Year', '').strip()
                        pub_month = ts.get('Month', '').strip()
                        pub_day = ts.get('Day', '').strip()
                    date = year + '/' + pub_month + '/' + pub_day
                    doi = ''
                    link = ''
                    aids = a.get('PubmedData', {}).get('ArticleIdList', {}).get('ArticleId', [])
                    pmid = ''
                    for aid in aids:
                        try:
                            if aid.get('@IdType', '') == 'doi':
                                doi = aid.get('#text', '').strip()
                            elif aid.get('@IdType', '') == 'pubmed':
                                pmid = str(aid.get('#text', '')).strip()
                                link = 'https://www.ncbi.nlm.nih.gov/pubmed/' + pmid
                            if len(doi) > 0 and len(link) > 0:
                                break
                        except:
                            pmid = str(aid).strip()
                            doi = str(doi).strip()
                    pmid = pmid.replace('#text', '')
                    if ('text' in doi or len(doi) == 0) and len(pmid) > 0:
                        doi = 'PubMed_' + pmid
                        link = 'https://www.ncbi.nlm.nih.gov/pubmed/' + pmid
                    if len(link) == 0 and len(doi) > 0:
                        link = 'https://doi.org/' + doi
                    if h.is_empty(doi):
                        _doi = tools.simplify_url(link)
                        if len(_doi) > 0:
                            doi = _doi
                        else:
                            doi = h.remove_nonalphanumeric(title, '').replace(' ', '_').lower()
                    try:
                        if len(year) == 4 and not (search.start_year <= int(year) <= search.end_year):
                            search.result_counts[ncbi.source]['invalid_years'] += 1
                            continue
                    except:
                        search.result_counts[ncbi.source]['invalid_years'] += 1
                        continue

                    if search.exists_doi(doi, ncbi.source):
                        search.result_counts[ncbi.source]['duplicates'] += 1
                        continue
                    year = year[:4]
                    if num_diff_countries > num_authors:
                        num_diff_countries = num_authors
                    s_emails = ''
                    r = (
                        doi, link, title, authors, date, year, citations, abstract, keyword, ncbi.source,
                        publication_title,
                        affiliations, num_diff_affiliations, countries, num_diff_countries, num_authors, s_emails)
                    row = [h.remove_tabs(s) for s in r]
                    if len(row[7]) < Config.abstract_min_length:
                        search.result_counts[ncbi.source]['incomplete_data'] += 1
                        continue
                    data.append(row)
                    if i % 100 == 0:
                        h.log_line('Line', i, 'in file:', src_filename)
        except Exception as e:
            h.log_line('Error with', src_filename, e)
        h.log_line('Completed with file:', src_filename)
        return data

    @staticmethod
    def _crawl_keyword(folder_name, keyword, library_key: str = 'pubmed'):
        keyword_filename = os.path.join(folder_name, slugify(keyword) + '.xml')
        url_keyword = urllib.parse.quote_plus(keyword)
        download_url = ncbi.url_template.format(library_key, url_keyword)

        status = download_ncbi_files(download_url, folder_name, keyword_filename, library_key)
        if status:
            h.log_line("Downloaded succeeded for " + keyword, 'Downloaded to:', keyword_filename, 'Size (MB):',
                       round(os.stat(keyword_filename).st_size / 10 ** 6, 2))
        else:
            h.log_line("Could not download for " + keyword)
        # from ncbi_obsolete import ncbi_obsolete
        # ncbi_obsolete._download_xml(download_url, keyword, library_key, keyword_filename)
        return status
