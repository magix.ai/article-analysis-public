#!/usr/bin/env python3
import getopt
import json
import os
import sys
import time
from datetime import timedelta

import helpers as h
from acm import acm
from config import Config
from helpers import log_line
from ieeexplore import ieeexplore
from meta_analysis import process_articles, crawl_and_analyze, process_keywords
from ncbi import ncbi
from output_results import plot_all
from search_config import SearchConfig
from springer import springer


def version():
    print('3.0')


def usage():
    print('Usage:')
    print(
        ' python3 main.py -f folder')
    print()
    print('-h              Help')
    print('--help          Help')
    print('--pomos         Help')
    print('-v              Print version')
    print('--version       Print version')
    print(
        '-f              Folder for all results data (abs or relative path) or path to the pickle file for -a (Default: None - user will be asked)')
    print(
        '--repo          Repository folder where all downloaded data is kept. Default: the parent of the results folder')
    print(
        '--post_process  Post process and export results and charts (Default: False)')
    print('-p              Plot results')
    print('--plot          Plot results')
    print('--plot_type     Format of charts. pdf/png. Default: plot_type=pdf')
    print('-s              Start year (Default: current-9)')
    print('--start_year=X  Start year (Default: current-9)')
    print('-e              End year (Default: current')
    print('--end_year=Y    End year (Default: current)')
    print('--ieee          y/yes/n/no. Default: IEEE=yes')
    print('--pubmed        PubMed: y/yes/n/no. Default: pubmed=yes')
    print('--pmc           PubMedCentral: y/yes/n/no. Default: pmc=no')
    print('--springer      y/yes/n/no. Default: Springer=yes')
    print('--acm           y/yes/n/no. Default: ACM=no')
    print('--relevant      Minimum relevant properties. Default: relevant=3')
    print('--process       Process everything from scratch. This is the default behavior')
    print('--add_lin_coef  Add linear coefficients to charts (y/yes/n/no). Default: add_lin_coef=no')
    print(
        '--capitalize    Capitalize the properties in the charts. E.g. AI will be capitalized to Ai; System Design -> System design (y/yes/n/no). Default: capitalize=no')
    print('--stem          Stem properties and abstracts. (y/yes/n/no). Default: stem=yes')
    print(
        '--ask_to_replace  Whether to ask the user about confirmation to replace existing files (y/n/yes/no/true/false/1/0). Default ask_to_replace=true')


def main(argv):
    t0 = time.time()
    log_line('Arguments:', argv)
    try:
        opts, args = getopt.getopt(argv, 'hvf:rps:e:',
                                   ['help', 'pomos', 'version', 'post_process', 'plot', 'plot_type=',
                                    'pmc=', 'pubmed=', 'ieee=', 'springer=', 'capitalize=', 'acm=', 'start_year=',
                                    'end_year=', 'repo=', 'relevant=', 'ask_to_replace=',
                                    'add_lin_coef=', 'stem='])

        log_line('Parsed arguments:', opts)
    except Exception as e:
        log_line('Exception:', e)
        usage()
        sys.exit(2)
    post = False
    path = None
    pmc = False
    pubmed = True
    ieee = True
    springer = True
    acm = False
    start_year = None
    end_year = None
    plot = False
    plot_type = 'pdf'
    folder_repo = None
    relevant = Config.min_properties_relevant
    ask_to_replace = True
    add_lin_coef = False
    stem = True
    capitalize = False
    for opt, arg in opts:
        try:
            if opt in ('-h', '--help', '--pomos'):
                usage()
                sys.exit()
            elif opt in ('-v', '--version'):
                version()
                sys.exit()
            elif opt in ('-f'):
                path = arg
            elif opt in ('--post_process'):
                post = True
            elif opt in ('--plot', '-p'):
                plot = True
            elif opt in ('--plot_type'):
                plot_type = 'png' if 'png' in arg.lower() else 'pdf'
            elif opt in ('--start_year', '-s'):
                start_year = int(arg)
            elif opt.lower() in ('--repo'):
                folder_repo = arg
            elif opt in ('--end_year', '-e'):
                end_year = int(arg)
            elif opt.lower() in ('--pubmed'):
                pubmed = not 'n' in arg.lower()
            elif opt.lower() in ('--capitalize'):
                capitalize = not 'n' in arg.lower()
            elif opt.lower() in ('--pmc'):
                pmc = not 'n' in arg.lower()
            elif opt.lower() in ('--ieee'):
                ieee = not 'n' in arg.lower()
            elif opt.lower() in ('--springer'):
                springer = not 'n' in arg.lower()
            elif opt.lower() in ('--acm'):
                acm = not 'n' in arg.lower()
            elif opt.lower() in ('--stem'):
                stem = not 'n' in arg.lower()
            elif opt.lower() in ('--relevant'):
                relevant = int(arg)
            elif opt.lower() in ('--add_lin_coef'):
                add_lin_coef = arg.lower() in ('yes', 'true', 'y', '1')
            elif opt.lower() in ('--ask_to_replace'):
                ask_to_replace = arg.lower() not in ('no', 'false', '0', 'n')
        except Exception as e:
            log_line('Exception:', opt, arg, e)
    if os.path.isfile(path):
        folder = h.get_parent_folder(path)
        filename = path
    else:
        folder = path
        filename = os.path.join(path, 'search_data.pickle')
    h.log_line('Plot:', plot, 'Plot type:', plot_type)

    search = SearchConfig.prepare_search_object(filename, ieee=ieee, pmc=pmc, pubmed=pubmed, springer=springer, acm=acm,
                                                folder_repo=folder_repo, relevant=relevant, add_lin_coef=add_lin_coef,
                                                plot_type=plot_type, capitalize=capitalize, stem=stem,
                                                start_year=start_year, end_year=end_year)
    if post:
        process_articles(search)
    elif plot:
        plot_all(search)
    else:
        crawl_and_analyze(search)

    h.safe_exec_bash_cmd(['bash', '~/crawler/article-analysis/commit_results_specific.sh', folder])
    dur = (time.time() - t0)
    dur_ts = timedelta(seconds=dur)
    h.log_line('Done. Duration:', dur_ts, 'Duration in seconds:', dur)


if __name__ == "__main__":
    main(sys.argv[1:])
