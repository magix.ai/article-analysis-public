#!/usr/bin/env python3
from output_results import generate_prisma_plots
from search_config import SearchConfig
from main import crawl_and_analyze
from meta_analysis import process_articles

def process_1k_articles_new_keywords():
    s = SearchConfig.prepare_search_object(
        filename='/Users/eftim/Repositories/article-analysis/examples/Aging_Policies_original_data_new_keywords/Aging_Policies_keywords_goce.json')
    fn_excel = '/Users/eftim/Repositories/article-analysis/examples/Aging_Policies_original_data_new_keywords/Aging_Policies_articles.xlsx'
    # df = pd.read_excel(fn_excel, sep='\t')
    # process_articles(s, df)
    generate_prisma_plots(s,
                          '/Users/eftim/Repositories/article-analysis/examples/Aging_Policies_original_data_new_keywords/results/aggr_prisma_manual.csv')


def process_70_articles_new_keywords():
    s = SearchConfig.prepare_search_object(
        filename='/Users/eftim/Repositories/article-analysis/examples/Aging_Policies_filtered_data_new_keywords/Aging_Policies_keywords_goce.json')
    fn_excel = '/Users/eftim/Repositories/article-analysis/examples/Aging_Policies_filtered_data_new_keywords/Filtered_property_groups_YellowMarkedOnly.xlsx'
    # df = pd.read_excel(fn_excel, sep='\t')
    # process_articles(s, df)
    generate_prisma_plots(s,
                          '/Users/eftim/Repositories/article-analysis/examples/Aging_Policies_filtered_data_new_keywords/results/aggr_prisma_manual.csv')


def process_70_articles_old_keywords():
    s = SearchConfig.prepare_search_object(
        filename='/Users/eftim/Repositories/article-analysis/examples/Aging_Policies_filtered_data_old_keywords/Aging_Policies_keywords_goce.json')
    fn_excel = '/Users/eftim/Repositories/article-analysis/examples/Aging_Policies_filtered_data_old_keywords/Filtered_property_groups_YellowMarkedOnly.xlsx'
    # df = pd.read_excel(fn_excel, sep='\t')
    # process_articles(s, df)
    generate_prisma_plots(s,
                          '/Users/eftim/Repositories/article-analysis/examples/Aging_Policies_filtered_data_old_keywords/results/aggr_prisma_manual.csv')


def test_serde():
    folder = '/Users/eftim/Repositories/article-analysis/examples/Goncalo'
    filename = '/Users/eftim/Repositories/article-analysis/examples/Goncalo/search_data_input.json'
    folder_repo = '/Users/eftim/Repositories/article-analysis/examples/REPO'
    # search = SearchConfig(_folder=folder)
    # search.save_keywords()
    search2 = SearchConfig.load_object(filename)
    search2.filename_json = 'search_data_input3.json'
    search2.save_keywords()
    return
    # crawl_and_analyze(search)

def test_crawl():
    filename = '/Users/eftim/Repositories/article-analysis/examples/Goncalo/search_data_input.json'
    search = SearchConfig.load_object(filename)
    crawl_and_analyze(search)

def test_process():
    filename = '/Users/eftim/Repositories/article-analysis/examples/Goncalo/search_data.pickle'
    search = SearchConfig.from_pickle(filename)
    process_articles(search)


if __name__ == "__main__":
    # main(sys.argv[1:])
    # process_1k_articles_new_keywords()
    # process_70_articles_new_keywords()
    # process_70_articles_old_keywords()
    # test_crawl()
    test_process()
    # test_serde()
