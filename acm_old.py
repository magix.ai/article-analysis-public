from tools import tools
from config import *
import urllib
import helpers as h
from search_config import SearchConfig
from slugify import slugify
import csv, re, html
import re
from ieeexplore import ieeexplore
from browser_emulate import download_acm_files
from mitpress import mitpress
import lxml


class acm(object):
    url_template_download = 'http://dl.acm.org/exportformats_search.cfm?query={0}&filtered=&within=owners%2Eowner%3DHOSTED&dte={1}&bfr={2}&srt=%5Fscore&expformat={3}'
    # https://dl.acm.org/results.cfm?query=(eftim%20zdravevski)&within=owners.owner=HOSTED&filtered=&dte=1947&bfr=2018
    url_template_search = 'https://dl.acm.org/results.cfm?query=({0})&within=owners.owner=HOSTED&filtered=&dte={1}&bfr={2}'
    source = 'ACM'

    @staticmethod
    def crawl(search: SearchConfig, keyword: str = None):
        h.log_line('Crawling ACM Xplore...')
        search.result_counts[acm.source] = {}
        if keyword:
            acm._process_keyword(search, keyword)
        else:
            for keyword in search.search_data['keywords']['final']:
                acm._process_keyword(search, keyword)
        search.to_pickle()

    @staticmethod
    def _process_keyword(search: SearchConfig, keyword: str):
        acm._crawl_keyword(search.folder_ACM, search.folder_ACM_temp, keyword, search.start_year, search.end_year)
        acm._clean_result(search, keyword)

    @staticmethod
    def _clean_result(search: SearchConfig, keyword=None):
        h.log_line('Cleaning downloaded ACM CSV files...')
        search.result_counts.setdefault(acm.source, {})
        search_stats_header = ['considered', 'invalid_years', 'incomplete_data', 'duplicates', 'remaining', 'relevant']
        for metric in Config.search_stats_header[:-1]:  # except 'relevant', which is calculated later
            search.result_counts[acm.source].setdefault(metric, 0)
        if keyword:
            acm._clean_result_keyword(search, keyword)
            acm._merge_bibtex_and_cleaned_csv(search, keyword)
        else:
            for keyword in search.search_data['keywords']['final']:
                acm._clean_result_keyword(search, keyword)
                acm._merge_bibtex_and_cleaned_csv(search, keyword)

    @staticmethod
    def _clean_result_keyword(search: SearchConfig, keyword):
        src_filename = os.path.join(search.folder_ACM, slugify(keyword) + '.csv')
        ids = set()
        try:
            data = []
            with open(src_filename, 'r', encoding='utf8') as fp:
                for i, row in enumerate(csv.reader(fp, delimiter=',')):
                    if i > 0:
                        search.result_counts[acm.source]['considered'] += 1
                        if row[6] in ids:
                            search.result_counts[acm.source]['duplicates'] += 1
                            continue
                        ids.add(row[6])
                        if str(row[13]):
                            date = str(row[13])
                        else:
                            date = '{date}'
                        vals = [row[6], row[2], row[18], row[11]]
                        title, authors, year, doi = (html.unescape(s) for s in vals)
                        authors = authors.replace(';', ',')
                        num_authors = authors.count(',') + 1
                        if h.is_empty(doi):
                            doi = h.remove_nonalphanumeric(title)
                        abstract, citations, link = '{abstract}', '{citations}', '{link}'
                        year = year[:4]
                        if len(year) == 4 and not (search.start_year <= int(year) <= search.end_year):
                            search.result_counts[acm.source]['invalid_years'] += 1
                            continue
                        # TODO: implement this
                        publication_title, affiliations, num_diff_affiliations, countries, num_diff_countries = '', '', 0, '', 0
                        if num_diff_countries > num_authors:
                            num_diff_countries = num_authors
                        fields = [doi, link, title, authors, date, year, citations, abstract, keyword, acm.source,
                                  publication_title, affiliations, num_diff_affiliations, countries, num_diff_countries,
                                  num_authors]
                        data.append(fields)
            data = [Config.keywords_result_header] + data
            dst_filename = os.path.join(search.folder_ACM, slugify(keyword) + '_clean.csv')
            h.write_csv_file(data, dst_filename, separator='\t')

        except Exception as ex:
            h.log_line(ex)
            pass

    @staticmethod
    def _crawl_keyword(folder_name, folder_name_temp, keyword, start_year, end_year):
        keyword_filename = os.path.join(folder_name, slugify(keyword))
        url_keyword = urllib.parse.quote(keyword)
        download_url_bib = acm.url_template_download.format(url_keyword, start_year, end_year, 'bibtex')
        download_url_csv = acm.url_template_download.format(url_keyword, start_year, end_year, 'csv')
        # tools.download_file_simple(download_url_bib, keyword_filename + '.bib')
        # tools.download_file_simple(download_url_csv, keyword_filename + '.csv')
        search_url = acm.url_template_search.format(url_keyword, start_year, end_year)
        download_acm_files(search_url, folder_name_temp, keyword_filename + '.bib', keyword_filename + '.csv')

    re_abstract = re.compile(r'ABSTRACT.*<p>(.*)</p>.*AUTHORS', re.MULTILINE | re.DOTALL)
    re_paragraph = re.compile(r'<p[^>]+>(.*)</p>')
    re_number = re.compile(r'\d+')
    _publication_title_xpaths = [
        '//*[@id="divmain"]/table/tbody/tr/td[1]/table[3]/tbody/tr/td/table/tbody/tr[4]/td/text()',
        '//*[@id="divmain"]/table[2]/tbody/tr/td/table/tbody/tr[3]/td/text()',
        '//*[@id="divmain"]/table[2]/tbody/tr/td/table/tbody/tr[4]/td/text()']

    @staticmethod
    def _get_publication_type(web_page):
        try:
            citations = tools.between(web_page, "Citation Count:", "Downloads")
            m = acm.re_number.search(citations)
            if m:
                citations = str(m.group())
        except:
            citations = '0'
        return citations

    @staticmethod
    def _get_abstract(web_page):
        abstract = ''
        s = tools.between(web_page, 'ABSTRACT', 'AUTHORS')
        s = h.remove_tabs(s)
        m = acm.re_paragraph.search(s)
        if m:
            abstract = m.group(1)
            abstract = html.unescape(abstract)
        return abstract

    @staticmethod
    def _get_citations(web_page):
        try:
            citations = tools.between(web_page, "Citation Count:", "Downloads")
            m = acm.re_number.search(citations)
            if m:
                citations = str(m.group())
        except:
            citations = '0'
        return citations

    @staticmethod
    def _merge_bibtex_and_cleaned_csv(search: SearchConfig, keyword: str):
        filename_csv_links = os.path.join(search.folder_ACM, slugify(keyword) + '_links.csv')
        try:
            filename_csv_clean = os.path.join(search.folder_ACM, slugify(keyword) + '_clean.csv')
            filename_bibtex = os.path.join(search.folder_ACM, slugify(keyword) + '.bib')
            # use it here so if someone deletes the articles, the redirects are preserved
            filename_redirects = os.path.join(search.folder_ACM, slugify(keyword) + '_redirects.json')

            csv_list, csv_header = h.read_csv_file(filename_csv_clean, 'str', True, separator='\t', has_header=True)
            bibtex_list = h.read_bibtex_file(filename_bibtex)
            bib_dois = dict([(h.remove_nonalphanumeric(r['doi'], ''), r) for r in bibtex_list if
                             not h.is_empty(r.get('doi', ''))])
            try:
                known_redirects = h.from_json(json_file=filename_redirects)
            except:
                known_redirects = {}
            unknown_domains = set()
            unknown_domain_links = []
            data = []
            skipped = 0
            i = 0
            if len(csv_list) == 0 or len(bibtex_list) == 0:
                raise Exception('No data to process.', len(csv_list), len(bibtex_list))
            for r in csv_list:
                # use this list so articles are processed in order of search results
                doi = h.remove_nonalphanumeric(r[0], '')
                try:
                    bib = bib_dois.get(doi, None)
                    if bib:
                        if i > Config.max_papers_acm:
                            h.log_line('Terminating ACM crawling. Max reached:', Config.max_papers_acm)
                            break
                        i += 1
                        if search.exists_doi(doi, acm.source):
                            search.result_counts[acm.source]['duplicates'] += 1
                            continue
                        abstract = ''
                        citations = '0'
                        url = bib['url'].strip()
                        if search.exists_url(url, acm.source):
                            search.result_counts[acm.source]['duplicates'] += 1
                            continue
                        url = known_redirects.get(url, url)
                        if search.exists_url(url, acm.source):
                            search.result_counts[acm.source]['duplicates'] += 1
                            continue
                        # TODO: implement these
                        publication_title, affiliations, num_diff_affiliations, countries, num_diff_countries = '', '', 0, '', 0
                        filename_page = tools.html_filename(url, search.folder_ACM_Articles)
                        page, final_url = tools.download_page(url, filename_page, True)
                        if final_url != url:
                            if 'dl.acm.org' in final_url:
                                final_url += '&preflayout=flat'
                            known_redirects[url] = final_url
                        if final_url != url:
                            if search.exists_url(final_url, acm.source):
                                search.result_counts[acm.source]['duplicates'] += 1
                                continue
                            filename_page = tools.html_filename(final_url, search.folder_ACM_Articles)
                            page, final_url = tools.download_page(final_url, filename_page, True)
                            publication_title = None
                        if 'dl.acm.org' in final_url:
                            abstract = acm._get_abstract(page)
                            citations = acm._get_citations(page)
                            publication_title = None
                            for xp in acm._publication_title_xpaths:
                                web_page = lxml.html.fromstring(page)
                                publication_title = web_page.xpath(xp)
                                if publication_title: break
                            publication_title = tools._clean_string(publication_title)
                        elif 'ieeexplore.ieee.org/document' in final_url or 'ieeexplore.ieee.org/stamp/stamp.jsp?arnumber=' in final_url:
                            abstract, citations = ieeexplore.get_abstract_citations(page)
                        elif 'www.mitpressjournals.org' in final_url:
                            abstract = mitpress.get_abstract_mitpress(page)
                        else:
                            unknown_domain_links.append(final_url)
                            unknown_domains.add(urllib.parse.urlsplit(url).hostname)
                        r[1] = final_url
                        r[7] = abstract
                        r[6] = citations
                        if h.is_emptyz(r[10]):
                            r[10] = publication_title
                        if h.is_emptyz(r[11]):
                            r[11] = affiliations
                        if h.is_emptyz(r[12]):
                            r[12] = num_diff_affiliations
                        if h.is_emptyz(r[13]):
                            r[13] = countries
                        if h.is_emptyz(r[14]):
                            r[14] = num_diff_countries
                        row = [h.remove_tabs(s) for s in r]
                        if len(row[7]) < Config.abstract_min_length:
                            search.result_counts[acm.source]['incomplete_data'] += 1
                            continue
                        data.append(row)  # only these rows will be included
                    else:
                        search.result_counts[acm.source]['incomplete_data'] += 1
                        h.log_line('Skipping article (no match in bibtex):', r[0], r[1])
                        skipped += 1
                except Exception as ex:
                    h.log_line(ex)
                    pass
            h.log_line('Total skipped articles:', skipped)
            h.to_json(known_redirects, json_file=filename_redirects)
            search.write_results(data, filename_csv_links)
            h.write_csv_file(unknown_domains, os.path.join(search.folder_ACM, 'unknown_domains.csv'))
            h.write_csv_file(unknown_domain_links, os.path.join(search.folder_ACM, 'unknown_domain_links.csv'))
        except Exception as e:
            h.log_line('Error', keyword, e)
            search.write_results(None, filename_csv_links)
