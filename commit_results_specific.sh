#!/usr/bin/env bash
folder=$1
cur_fold=`pwd`

cd ~/crawler/article-analysis
results_name=`basename $folder`
echo Processing: $folder/results*
for results_folder in $folder/results* ;
do
    if [[ -d $results_folder ]]; then
        echo Processing folder: $results_folder
        archive=`basename $results_folder`_${results_name}.zip
        echo Archive with results: $archive
        if [[ ! -f $archive ]]; then
            echo Archiving to $archive
            cd $results_folder
            for image_file in `ls *pdf` ; do
                image_file_out=${image_file}_cr.pdf
                pdfcrop --margins 5 $image_file $image_file_out
                mv $image_file_out $image_file
            done
            for image_file in `ls *pdf` ; do echo $image_file; done
            zip $archive aggr*pdf aggr*png *prisma* aggr*csv aggr*xlsx *bib *txt *json plot* filtered_articles.xlsx filtered_graph*spring* filtered_graph*gml filtered_graph_data.csv articles_stemmed.csv
        fi
        ls -lah $archive
        echo Moving: mv $archive ~/crawler/survey-results/results/
        mv $archive ~/crawler/survey-results/results/
        cd ~/crawler/survey-results/results
        git add -f $archive
        echo Current result files
        ls -lah *
        echo "Pushing results to survey-results repo for $archive"
        git commit -m "$archive"
    fi
done
cd ~/crawler/survey-results && git push

cd $cur_fold
