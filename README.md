# README #


### What is this repository for? ###

* The description is available in the papers mentioned below, particularly https://link.springer.com/chapter/10.1007/978-3-030-10752-9_1
* Disclaimer: As of January 23, 2021, only the Springer parser works as the websites of IEEE Xplore, ACM and NCBI have been updated, so the scraper no longer can process them.

### How do I get set up? ###

* python3 is required
* Configuration
* Dependencies: see ```requirements.txt```
* Deployment instructions: see `install_selenium_ubuntu.sh` and `setup_linux_git.sh`

### Licence ###

* CC BY-NC-SA: This license lets others remix, adapt, and build upon your work non-commercially, as long as they credit you and license their new creations under the identical terms.

#### Additionally we ask for you to cite the following papers:
- Eftim Zdravevski, Petre Lameski, Vladimir Trajkovik, Ivan Chorbev, Rossitza Goleva, Nuno Pombo, Nuno M. Garcia, Automation in Systematic, Scoping and Rapid Reviews by an NLP Toolkit: A Case Study in Enhanced Living Environments, https://link.springer.com/chapter/10.1007/978-3-030-10752-9_1
- Tatjana Loncar-Turukalo, Eftim Zdravevski, José Machado da Silva, Ioanna Chouvarda, Vladimir Trajkovik, Literature on Wearable Technology for Connected Health: Scoping Review of Research Trends, Advances, and Barriers, https://www.jmir.org/2019/9/e14017/?fbclid=IwAR2c2KL5DDyrVvm5CHwLBGIyZgKbp9pQj47S3fEekVWrlqgEQ_rYWC8F3mo&utm_source=TrendMD&utm_medium=cpc&utm_campaign=JMIR_TrendMD_1 

#### Other papers leveraging this framework:
- Literature on Applied Machine Learning in Metagenomic Classification: A Scoping Review, https://www.mdpi.com/2079-7737/9/12/453
- Aging at Work: A Review of Recent Trends and Future Directions, https://www.mdpi.com/1660-4601/17/20/7659

### Who do I talk to? ###

* Eftim Zdravevski: eftim.zdravevski [AT] finki [DOT] ukim [DOT] mk
