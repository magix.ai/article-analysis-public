#!/usr/bin/env python3
import os, sys
import glob


def path_leaf(path):
    import ntpath
    head, tail = ntpath.split(path)
    return tail or ntpath.basename(head)


def get_parent_folder(cur_path):
    return os.path.dirname(os.path.realpath(cur_path))


if __name__ == "__main__":
    if len(sys.argv) > 1:
        parent_folder = sys.argv[1]
    else:
        parent_folder = os.getcwd()

    parent_folder = '/Users/eftim/Pictures/exported videos till 2019-05-13'
    # parent_folder = '/Users/eftim/Pictures/test export 2019-05-13'
    folders = glob.glob(os.path.join(parent_folder, '*'))
    cnt = 0
    for d in folders:
        if os.path.isfile(d):
            continue
        print('Processing folder:', d)
        files = glob.glob(os.path.join(d, '*'))
        folder_name = path_leaf(d)
        for f in files:
            filename = path_leaf(f)
            fn, ext = os.path.splitext(filename)
            new_filename = os.path.join(d, folder_name + ' ' + filename)
            print('Renaming: ', f, ' => ', new_filename)
            os.rename(f, new_filename)
            cnt += 1
    print('Renamed:', cnt, 'files')
