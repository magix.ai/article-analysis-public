import re
import time
import urllib

import html2text
from lxml import etree
from slugify import slugify

import helpers as h
from config import *
from countries import Countries
from search_config import SearchConfig
from tools import tools
import os

class springer(object):
    # CONSTANTS
    url_template = 'https://link.springer.com/search/page/{0}?query={1}&date-facet-mode=between&facet-start-year={2}&facet-end-year={3}'
    absolute_uri = 'https://link.springer.com'

    @staticmethod
    def crawl(search: SearchConfig, keyword: str = None):
        search.set_status('Springer', 'Starting')
        h.log_line('Crawling Springer...')
        search.result_counts[Config.sources['springer']] = {}
        if keyword:
            springer._process_keyword(search, keyword)
        else:
            for keyword in search.keywords:
                springer._process_keyword(search, keyword)
        search.set_status('Springer')
        search.to_pickle()

    @staticmethod
    def _process_keyword(search: SearchConfig, keyword: str):
        links = springer._crawl_keyword(search.folder_Springer_Links, keyword, search.start_year,
                                        search.end_year)
        dst_filename = os.path.join(search.folder_Springer, slugify(keyword) + '_links.csv')

        search.set_result_counts(Config.sources['springer'])
        data = []
        for i, link in enumerate(links):
            fields = springer._scrape_article_page(search, link, keyword)
            if fields:
                data.append(fields)
        search.write_results(data, dst_filename)

    @staticmethod
    def _crawl_keyword(folder_name, keyword, start_year, end_year):
        keyword_filename = os.path.join(folder_name, slugify(keyword))
        keyword_filename_template = keyword_filename + '_page_{0}.txt'
        keyword_filename += '_urls_only.csv'
        links = []
        if os.path.isfile(keyword_filename) and os.stat(keyword_filename).st_size > 0:
            h.log_line('File exists:', keyword_filename, 'Skipping Springer download of', keyword)
            links = h.read_csv_file(keyword_filename, 'str', True, [0], '\t', True)
        url_keyword = urllib.parse.quote_plus(keyword)
        download_url = springer.url_template.format("{0}", url_keyword, start_year, end_year)
        try:
            data = []
            links = set()
            for page in range(1, Config.number_of_pages_springer + 1):
                current_url = download_url.format(page)
                web_page, current_url = tools.download_page(current_url, keyword_filename_template.format(page))
                all_links_on_page = web_page.xpath('//a/@href')
                article_links = springer._filter_links(all_links_on_page, "/article/")
                chapter_links = springer._filter_links(all_links_on_page, "/chapter/")
                new_links = article_links + chapter_links
                if len(new_links) == 0:
                    h.log_line('Breaking at page', page, 'because no new links were found.', 'Total links:', len(links))
                    break
                h.log_line('Page', page, 'of', Config.number_of_pages_springer, 'finished. Total links so far:',
                           len(links))
                for l in new_links:
                    if l not in links:
                        links.add(l)
                        data.append([page, l, current_url, keyword, Config.sources['springer']])
                if len(links) >= Config.max_papers_springer:
                    h.log_line('Breaking at page', page, 'because max papers:', Config.max_papers_springer,
                               'was reached.', 'Total links:', len(links))
                    break
            header = ['page', 'link', 'page_link', 'keyword', 'source']
            data = [header] + data
            h.write_csv_file(data, keyword_filename, separator='\t')

        except Exception as ex:
            h.log_line(ex)
            pass
        return links

    @staticmethod
    def _filter_links(all_links, relative_url):
        links = [springer.absolute_uri + x for x in all_links if
                 relative_url in x and '.jpg' not in x and '.png' not in x and '.html' not in x and '.htm' not in x]
        return links

    _title_xpaths = ['//*[@id="kb-nav--main"]/div[1]/div/h1/text()',
                     '//*[@id="main-content"]/div/div/article/div/div[1]/div[2]/h1/text()',
                     '//*[@id="main-content"]/article/div/div/div/div[2]/div[1]/div[2]/h1/text()',
                     '//*[@id="main-content"]/article/div/div/div/div[1]/div[1]/div[2]/h1/text()',
                     '//*[@id="main-content"]/article/div/div/div/div[3]/div[1]/div[2]/h1/text()']
    _abstract_xpaths = ['//*[@id="Abs1"]/p/text()', '//*[@id="Par1"]/text()', '//*[@id="ASec1"]/p/text()',
                        '//*[@id="Sec1"]/div/p/text()', '//*[@id="body"]/p/text()', '//*[@id="Abs1_6"]/p/text()',
                        '//*[@id="Abs10"]/p/text()']
    _authors_xpaths = ['//*[@id="kb-nav--main"]/div[1]/ul/li/span/span[1]/text()',
                       '//*[@id="authors"]/ul/span[1]/text()',
                       '//*[@id="authorsandaffiliations"]/div/ul/li/span[1]/text()',
                       '//*[@id="authorsandaffiliations"]/div/ul/li/text()']
    _email_xpaths = ['//*[@id="authorsandaffiliations"]/div/ul/li[1]/span[2]/span[1]/a/@href',
                     '//*[@id="authorsandaffiliations"]/div/ul/li[1]/span[2]/span/a/@href']
    _affil_xpaths = [
        # '//*[@id="authorsandaffiliations"]/div',
        '//*[@id="authorsandaffiliations"]/div/ol'
        #                 '//*[@id="authorsandaffiliations"]/div/ol/li[3]'
    ]
    _citations_xpaths = ['//*[@id="citations-count-number"]/text()', '//*[@id="citations-link"]/span[1]/text()',
                         '//*[@id="chaptercitations-link"]/span[1]/text()',
                         '//*[@id="chaptercitations-count-number"]/text()',
                         '//*[@id="citations-count-number"]/span/text()']
    _date_xpaths = ['//*[@id="article-dates-history"]/div[2]/span[2]/time',
                    '//*[@id="enumeration"]/div/span[1]/time/text()',
                    '//*[@id="main-content"]/article/div/div/div/div[2]/div[1]/div[4]/div[1]/div/dl[1]/dd/time/text()',
                    '//*[@id="main-content"]/article/div/div/div/div[2]/div[1]/div[4]/div[1]/div/dl/dd/a/text()',
                    '//*[@id="main-content"]/article/div/div/div/div[3]/div[1]/div[4]/div[1]/div/dl/dd/a/text()',
                    '//*[@id="main-content"]/article/div/div/div/div[3]/div[1]/div[4]/div[1]/div/dl[1]/dd/time/text()',
                    '//*[@id="main-content"]/article/div/div/div/div[1]/div[1]/div[4]/div[1]/div/dl[1]/dd/time/text()',
                    '//*[@id="main-content"]/article/div/div/div/div[2]/div[1]/div[4]/div[1]/div/dl[0]/dd[0]/a/text()',
                    '//*[@class="gtm-first-online"]/text()'
                    '//*[@id="main-content"]/div/div/article/div/div[1]/div[4]/div[1]/div/div[1]/span[2]/a/text()'
                    ]
    _doi_xpaths = ['//*[@class="article-doi"]/p/text()',
                   '//*[@id="main-content"]/article/div/div/div/div[3]/div[1]/div[4]/div[1]/p/text()',
                   '//*[@id="main-content"]/article/div/div/div/div[2]/div[1]/div[4]/div[1]/p/text()',
                   '//*[@id="AboutThisContent"]/div/div[2]/ul[1]/li[1]/span[2]/text()',
                   '//*[@id="AboutThisContent"]/div/div/ul[1]/li[1]/span[2]/text()']

    _copyright_xpaths = ['//*[@id="main-content"]/div/div/article/div/section[4]/div/div/text()',
                         '//*[@id="main-content"]/div/div/article/div/section[3]/div/div/text()']
    _publication_type_xpaths = ['//*[@id="enumeration"]/p[1]/a/span/text()']

    _re_year_cite = re.compile(r'\((\d+)\)')

    @staticmethod
    def _scrape_article_page(search: SearchConfig, url, keyword=None):
        search.result_counts.setdefault(Config.sources['springer'], {})
        for metric in Config.search_stats_header[:-1]:  # except 'relevant', which is calculated later
            search.result_counts[Config.sources['springer']].setdefault(metric, 0)
        search.result_counts[Config.sources['springer']]['considered'] += 1
        filename = tools.html_filename(url, search.folder_Springer_Articles)
        fields = None
        try:
            web_page, final_url = tools.download_page(url, filename)
            # keyword is None means we're testing, so don't check duplicates if this is a test
            if keyword and search.exists_url(final_url, Config.sources['springer']):
                search.result_counts[Config.sources['springer']]['duplicates'] += 1
                return fields
            title = None
            for xp in springer._title_xpaths:
                title = web_page.xpath(xp)
                if title: break
            abstract = None
            for xp in springer._abstract_xpaths:
                abstract = web_page.xpath(xp)
                if abstract: break

            authors = None
            for xp in springer._authors_xpaths:
                authors = web_page.xpath(xp)
                if authors: break
            l_authors = []
            for _auth in authors:
                s_auth = str(_auth)  # etree.tostring(_auth, pretty_print=True).decode('utf8')
                s_auth = h.remove_tabs(s_auth.lstrip("0123456789. \t").replace(',', ' ').replace(';', ' '))
                if not h.is_empty(s_auth):
                    l_authors.append(s_auth)
            authors = ','.join(l_authors)
            emails = []
            for xp in springer._email_xpaths:
                email = web_page.xpath(xp)
                email = str(email).strip("[]'").replace('mailto:', '')
                email = h.extract_email(email)
                if not h.is_empty(email):
                    emails.append(email)
                    break
            s_affil = None
            for xp in springer._affil_xpaths:
                s_affil = web_page.xpath(xp)
                if s_affil: break
            l_affil = []
            for i, ca in enumerate(s_affil):
                html_affil = etree.tostring(ca, pretty_print=True).decode('utf8')
                _lc_affil = html2text.html2text(html_affil).split('\n')
                for j, c_affil in enumerate(_lc_affil):
                    c_affil = h.remove_tabs(c_affil.lstrip("0123456789. \t").replace(';', ','))
                    if not h.is_empty(c_affil):
                        l_affil.append(c_affil)
            # print(len(l_affil), l_affil)
            s_affil = ';'.join(l_affil)
            # return
            affiliations, num_diff_affiliations, countries, num_diff_countries = Countries.process_affiliations(s_affil)
            # print(num_diff_affiliations, countries, num_diff_countries, affiliations)
            # return
            citations = None
            for xp in springer._citations_xpaths:
                citations = web_page.xpath(xp)
                if citations: break

            date = None
            for xp in springer._date_xpaths:
                date = web_page.xpath(xp)
                if date: break
            date = tools._clean_string(date)
            year = date[-4:] if date else ''
            if h.is_empty(year):
                citeas = web_page.xpath('//*[@id="citethis-text"]/text()')
                citeas = tools._clean_string(citeas)
                if len(citeas) > 4:
                    yearm = springer._re_year_cite.search(citeas)
                    if yearm:
                        year = yearm.group(1)
                        if h.is_empty(date):
                            date = year
            if h.is_empty(year):
                _copyright = None
                for xp in springer._copyright_xpaths:
                    _copyright = web_page.xpath(xp)
                    if _copyright: break
                _copyright = tools._clean_string(_copyright)
                year = h.remove_non_numeric(_copyright)
                if h.is_empty(date):
                    date = year

            doi = None
            for xp in springer._doi_xpaths:
                doi = web_page.xpath(xp)
                if doi: break

            doi = tools._clean_string(doi).replace('https://doi.org/', '')
            publication_title = None
            for xp in springer._publication_type_xpaths:
                publication_title = web_page.xpath(xp)
                if publication_title: break
            publication_title = tools._clean_string(publication_title)
            if h.is_empty(doi):
                _doi = tools.simplify_url(final_url)
                if len(_doi) > 0:
                    doi = _doi
                else:
                    doi = h.remove_nonalphanumeric(title, '')
            if keyword and search.exists_doi(doi, Config.sources['springer']):
                search.result_counts[Config.sources['springer']]['duplicates'] += 1
                return None
            title = tools._clean_string(title)
            abstract = tools._clean_string(abstract)
            authors = tools._clean_string(authors).replace(';', ',')
            num_authors = authors.count(',') + 1
            citations = tools._clean_string(citations)
            if not citations or citations == '':
                citations = 0

            if len(year) == 4 and not (search.start_year <= int(year) <= search.end_year):
                search.result_counts[Config.sources['springer']]['invalid_years'] += 1
                return fields
            if len(abstract) < Config.abstract_min_length:
                search.result_counts[Config.sources['springer']]['incomplete_data'] += 1
                return fields

            if h.is_empty(authors):
                search.result_counts[Config.sources['springer']]['incomplete_data'] += 1
                return fields

            if num_diff_countries > num_authors:
                num_diff_countries = num_authors
            s_emails = ';'.join(emails)
            if authors or title or abstract or date or doi:
                fields = (doi, final_url, title, authors, date, year, citations, abstract, keyword, Config.sources['springer'],
                          publication_title, affiliations, num_diff_affiliations, countries, num_diff_countries,
                          num_authors, s_emails)
            return fields
        except Exception as ex:
            h.log_line('Error url', url, ex)
            if not keyword:
                # we're testing, so re-raise the exception
                raise ex
            return fields
