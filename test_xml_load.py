#!/usr/bin/env python3
import helpers as h
from memory_profiler import profile
import xml.etree.ElementTree as ET


# import xml.etree.cElementTree as ET


@profile
def load_as_dict(src_filename):
    d = h.xml2dict(src_filename)
    n = len(d['PubmedArticleSet'].values())
    return n


@profile
def load_inc(src_filename):
    # http://effbot.org/zone/element-iterparse.htm
    # get an iterable
    context = ET.iterparse(src_filename, events=("start", "end"))

    # turn it into an iterator
    context = iter(context)
    # get the root element
    event, root = next(context)
    data = []
    num_elements = 0
    for event, elem in context:
        #     print(event, elem.tag)
        if event == "start" and elem.tag == "PubmedArticle":
            num_elements += 1
            article_title, abstract, authors, pub_title = '', '', '', ''
            pmid, doi = '', ''
            s_affiliations = None
            l_affiliations = []
            year = None
            citations = '-1'
            for event, elem in context:
                #     # print(num_elements, elem.tag, elem.text)
                if event == "end" and elem.tag == "PubmedArticle":
                    break
                if event != "start":
                    continue
                elif elem.tag == 'ArticleTitle':
                    article_title = elem.text
                    article_title = h.remove_tabs(article_title).strip()[:3000]
                elif elem.tag == 'Abstract':
                    abstract = elem.text
                    abstract = h.remove_nonalphanumeric(abstract).strip()[:3000]
                elif elem.tag == 'Title':
                    pub_title = elem.text
                    pub_title = h.remove_nonalphanumeric(pub_title).strip()[:3000]
                elif elem.tag == 'AuthorList':
                    last_names = []
                    first_names = []
                    for event_in, elem in context:
                        if event_in == "end" and elem.tag == "AuthorList":
                            break
                        if elem.tag == 'LastName':
                            last_names.append(elem.text)
                        elif elem.tag == 'ForeName':
                            first_names.append(elem.text)
                    try:
                        authorsl = [(fn + ' ' + ln).replace(',', ' ') for fn, ln in zip(first_names, last_names)]
                        authors = ', '.join(authorsl)
                    except:
                        authors = ''
                    authors = authors.replace(';', ',')
                    num_authors = authors.count(',') + 1
                    authors = h.remove_tabs(authors).strip()[:3000]
                elif not year and elem.tag in ('ArticleDate'):
                    for event_in, elem in context:
                        if event_in == "end" and elem.tag in ('ArticleDate'):
                            break
                        if elem.tag == 'Year':
                            year = elem.text
                        elif elem.tag == 'Month':
                            pub_month = elem.text
                        elif elem.tag == 'Day':
                            pub_day = elem.text
                elif not year and elem.tag in ('PubMedPubDate'):
                    for event_in, elem in context:
                        if event_in == "end" and elem.tag in ('PubMedPubDate'):
                            break
                        if elem.tag == 'Year' and elem.attrib.get('PubStatus', '') == 'accepted':
                            year = elem.text
                        elif elem.tag == 'Month' and elem.attrib.get('PubStatus', '') == 'accepted':
                            pub_month = elem.text
                        elif elem.tag == 'Day' and elem.attrib.get('PubStatus', '') == 'accepted':
                            pub_day = elem.text
                elif elem.tag == 'Affiliation':
                    l_affiliations.append(elem.text)
                elif elem.tag == 'ArticleId' and elem.attrib.get('IdType', '') == 'pubmed':
                    pmid = elem.text
                elif elem.tag == 'ArticleId' and elem.attrib.get('IdType', '') == 'doi':
                    doi = elem.text
            link = 'https://www.ncbi.nlm.nih.gov/pubmed/' + pmid
            if ('text' in doi or len(doi) == 0) and len(pmid) > 0:
                doi = 'PubMed_' + pmid
                link = 'https://www.ncbi.nlm.nih.gov/pubmed/' + pmid
            if len(link) == 0 and len(doi) > 0:
                link = 'https://doi.org/' + doi
            if h.is_empty(doi):
                _doi = tools.simplify_url(link)
                if len(_doi) > 0:
                    doi = _doi
                else:
                    doi = h.remove_nonalphanumeric(article_title, '').replace(' ', '_').lower()

            date = year + '/' + pub_month + '/' + pub_day
            year = year[:4]
            if len(l_affiliations) > 0:
                s_affiliations = ';'.join(l_affiliations)
            affiliations, num_diff_affiliations, countries, num_diff_countries = Countries.process_affiliations(s_affiliations)
            if num_diff_countries > num_authors:
                num_diff_countries = num_authors
            try:
                if len(year) == 4 and not (search.start_year <= int(year) <= search.end_year):
                    search.result_counts[ncbi.source]['invalid_years'] += 1
                    continue
            except:
                search.result_counts[ncbi.source]['invalid_years'] += 1
                continue
            if search.exists_doi(doi, ncbi.source):
                search.result_counts[ncbi.source]['duplicates'] += 1
                continue
            if len(row[7]) < Config.abstract_min_length:
                search.result_counts[ncbi.source]['incomplete_data'] += 1
                continue
            r = (
                doi, link, article_title, authors, date, year, citations, abstract, keyword, ncbi.source,
                pub_title,
                affiliations, num_diff_affiliations, countries, num_diff_countries, num_authors)
            row = [h.remove_tabs(s) for s in r]

            data.append(row)
            root.clear()
    return num_elements


# import timeit
# load_as_dict('/Users/eftim/Downloads/article-analysis/enhanced-living-environments.xml')
# load_inc('/Users/eftim/Downloads/article-analysis/enhanced-living-environments.xml')
load_as_dict('/Users/eftim/Downloads/article-analysis/connected-health.xml')
load_inc('/Users/eftim/Downloads/article-analysis/connected-health.xml')
