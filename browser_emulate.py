from selenium import webdriver
from selenium.webdriver.common import action_chains, keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time
import os
import helpers as h
import glob
from config import Config
import requests
from tools import tools
from time import sleep
from lxml import html


def create_chrome_browser(download_dir):
    options = webdriver.ChromeOptions()
    options.add_argument('--headless')
    options.add_argument('--nogui')
    options.add_argument('--disable-gpu')
    options.add_argument('--no-sandbox')
    options.add_experimental_option("prefs", {
        "download.default_directory": download_dir,
        "download.prompt_for_download": False,
        "download.directory_upgrade": True,
        "safebrowsing.enabled": True,
        "download_restrictions": 0
    })
    browser = webdriver.Chrome(chrome_options=options)

    return browser


def create_firefox_browser(download_dir):
    options = webdriver.FirefoxOptions()
    options.add_argument("--headless")

    profile = webdriver.FirefoxProfile()
    profile.set_preference("browser.download.folderList", 2)
    profile.set_preference("browser.download.manager.showWhenStarting", False)
    profile.set_preference("browser.download.dir", download_dir)
    profile.set_preference("browser.helperApps.neverAsk.saveToDisk",
                           'text/xml, text/csv, text/plain, application/x-bibtex')

    browser = webdriver.Firefox(firefox_profile=profile, firefox_options=options)
    # browser.implicitly_wait(2)  # wait for scripts to bind dynamic data
    return browser


def create_browser(download_dir):
    return create_firefox_browser(download_dir)
    # return create_chrome_browser(download_dir)


def download_file_selenium(url, download_dir, filename=None, wait_xpath='//body', return_string=False):
    if filename is None:
        filename = tools.html_filename(url)
    final_url = url
    if os.path.isfile(filename) and os.stat(filename).st_size > 0:
        h.log_line('File exists:', filename, 'Skipping download of', url)
        with open(filename, 'r', encoding="utf-8") as file:
            s = file.read()
    else:
        h.log_line('Downloading', url, 'to', filename)
        browser = create_browser(download_dir)
        browser.get(url)
        wait = WebDriverWait(browser, Config.browser_delay_time_acm)
        wait.until(EC.presence_of_element_located((By.XPATH, wait_xpath)))
        s = browser.page_source
        with open(filename, 'w+', encoding="utf-8") as f:
            f.write(s)
        browser.close()
    if return_string:
        web_page = s
    else:
        web_page = html.fromstring(s)
    return web_page, final_url


# h.log_line('Start')
# download_file_selenium('https://dl.acm.org/citation.cfm?id=3143728', '/Users/eftim/Downloads/article-analysis/data/acm/articles', '/Users/eftim/Downloads/article-analysis/df_data_sample/acm/articles/file2.html')
# h.log_line('Start1')
# download_file_selenium('https://dl.acm.org/results.cfm?query=(vladimir%20trajkovik)&within=owners.owner=HOSTED&filtered=&dte=2017&bfr=2018', '/Users/eftim/Downloads/article-analysis/data/acm/articles', '/Users/eftim/Downloads/article-analysis/df_data_sample/acm/articles/file1.html')
# h.log_line('Start2')

def download_ncbi_files(download_url, folder_name, filename, library_key):
    h.log_line(download_url, 'download', filename)
    status = True
    if os.path.isfile(filename) and os.stat(filename).st_size > 0:
        h.log_line('File', filename, 'exists. Size (MB):', round(os.stat(filename).st_size / 10 ** 6, 2),
                   'Skipping download.')
        return status
    search_form_filename = tools.html_filename(download_url, folder_name)
    search_form_result = tools.download_file_simple(download_url, search_form_filename)
    if 'No documents match your search terms' in search_form_result:  # r.text:
        h.log_line('No documents match the search term:', download_url)
        return False
    browser = create_browser(download_dir=folder_name)
    browser.get(download_url)
    wait = WebDriverWait(browser, Config.max_download_time_selenium)
    try:
        downloaded_file = os.path.join(folder_name, library_key + "_result.xml")
        if os.path.isfile(downloaded_file):
            os.remove(downloaded_file)
        elmSendTo = wait.until(EC.presence_of_element_located((By.ID, 'sendto')))
        elmSendTo.click()

        elmFileRadio = wait.until(
            EC.visibility_of_element_located((By.XPATH, "//input[@id='dest_File'][@type='radio']")))
        elmFileRadio.click()

        elmSelectXML = wait.until(EC.visibility_of_element_located((By.XPATH,
                                                                    "//div[@id='send_to_menu']/div[@id='submenu_File']/ul/li/select[@id='file_format']/option[@value='xml']")))
        elmSelectXML.click()

        elmBtnCreateFile = wait.until(EC.visibility_of_element_located((By.XPATH,
                                                                        "//div[@id='send_to_menu']/div[@id='submenu_File']/button[@type='submit'][@cmd='File']")))
        elmBtnCreateFile.click()
        slept = 0
        sleep_increment = 5
        prev_size = 0
        downloaded_file_part = downloaded_file + '.part'
        while True:
            h.log_line('Waiting for download to finish of:', downloaded_file, 'Elapsed time:', slept)
            time.sleep(sleep_increment)
            while os.path.isfile(downloaded_file_part) and os.stat(downloaded_file_part).st_size > 0:
                cur_size = os.stat(downloaded_file_part).st_size
                if cur_size > prev_size:
                    h.log_line('Elapsed:', slept, 'Sleeping while downloading:', filename, ':', downloaded_file_part,
                               'Size (MB) increased from:', prev_size * 1.0 / 10 ** 6,
                               'to', cur_size * 1.0 / 10 ** 6)
                    prev_size = cur_size
                    time.sleep(sleep_increment)
                    slept += sleep_increment
                else:
                    break

            if os.path.isfile(downloaded_file):
                h.log_line('It seems the result is downloaded to:', downloaded_file, 'Size (MB):',
                           os.stat(downloaded_file).st_size // 10 ** 6)
                break
            if slept > Config.max_download_time_selenium:
                h.log_line('Interrupting download of:', downloaded_file, 'after:', slept)
                break
            slept += sleep_increment
        if not os.path.exists(downloaded_file):
            h.log_line("File wasn't downloaded to:", downloaded_file, 'Stopping after:', slept, 'seconds.')
        else:
            os.rename(downloaded_file, filename)
    except Exception as e:
        h.log_line('Failed for:', download_url, e)
        h.write_csv_file(None, filename)
        status = False
    time.sleep(5)
    browser.close()
    return status

def download_ieeexplore_files(download_url, folder_name, filename):
    h.log_line(download_url, 'download', filename)
    status = True
    if os.path.isfile(filename) and os.stat(filename).st_size > 0:
        h.log_line('File', filename, 'exists. Size (MB):', round(os.stat(filename).st_size / 10 ** 6, 2),
                   'Skipping download.')
        return status
    search_form_filename = tools.html_filename(download_url, folder_name)
    search_form_result = tools.download_file_simple(download_url, search_form_filename)
    if 'No documents match your search terms' in search_form_result:  # r.text:
        h.log_line('No documents match the search term:', download_url)
        return False
    browser = create_browser(download_dir=folder_name)
    browser.get(download_url)
    wait = WebDriverWait(browser, Config.max_download_time_selenium)
    try:
        downloaded_file = os.path.join(folder_name, "result.xml")
        if os.path.isfile(downloaded_file):
            os.remove(downloaded_file)
        elmSendTo = wait.until(EC.presence_of_element_located((By.ID, 'sendto')))
        elmSendTo.click()

        elmFileRadio = wait.until(
            EC.visibility_of_element_located((By.XPATH, "//input[@id='dest_File'][@type='radio']")))
        elmFileRadio.click()

        elmSelectXML = wait.until(EC.visibility_of_element_located((By.XPATH,
                                                                    "//div[@id='send_to_menu']/div[@id='submenu_File']/ul/li/select[@id='file_format']/option[@value='xml']")))
        elmSelectXML.click()

        elmBtnCreateFile = wait.until(EC.visibility_of_element_located((By.XPATH,
                                                                        "//div[@id='send_to_menu']/div[@id='submenu_File']/button[@type='submit'][@cmd='File']")))
        elmBtnCreateFile.click()
        slept = 0
        sleep_increment = 5
        prev_size = 0
        downloaded_file_part = downloaded_file + '.part'
        while True:
            h.log_line('Waiting for download to finish of:', downloaded_file, 'Elapsed time:', slept)
            time.sleep(sleep_increment)
            while os.path.isfile(downloaded_file_part) and os.stat(downloaded_file_part).st_size > 0:
                cur_size = os.stat(downloaded_file_part).st_size
                if cur_size > prev_size:
                    h.log_line('Elapsed:', slept, 'Sleeping while downloading:', filename, ':', downloaded_file_part,
                               'Size (MB) increased from:', prev_size * 1.0 / 10 ** 6,
                               'to', cur_size * 1.0 / 10 ** 6)
                    prev_size = cur_size
                    time.sleep(sleep_increment)
                    slept += sleep_increment
                else:
                    break

            if os.path.isfile(downloaded_file):
                h.log_line('It seems the result is downloaded to:', downloaded_file, 'Size (MB):',
                           os.stat(downloaded_file).st_size // 10 ** 6)
                break
            if slept > Config.max_download_time_selenium:
                h.log_line('Interrupting download of:', downloaded_file, 'after:', slept)
                break
            slept += sleep_increment
        if not os.path.exists(downloaded_file):
            h.log_line("File wasn't downloaded to:", downloaded_file, 'Stopping after:', slept, 'seconds.')
        else:
            os.rename(downloaded_file, filename)
    except Exception as e:
        h.log_line('Failed for:', download_url, e)
        h.write_csv_file(None, filename)
        status = False
    time.sleep(5)
    browser.close()
    return status

def download_acm_files(download_url, folder_name_temp, filename_bib, filename_csv):
    h.log_line(download_url, 'download', filename_bib, filename_csv)
    status = True
    if os.path.isfile(filename_bib) and os.path.isfile(filename_csv) and os.stat(filename_bib).st_size > 0 and os.stat(
            filename_csv).st_size > 0:
        h.log_line('Files', filename_bib, filename_csv, 'exist. Skipping download.')
        return status
    browser = create_browser(download_dir=folder_name_temp)

    browser.get(download_url)
    wait = WebDriverWait(browser, Config.browser_delay_time_acm)
    try:
        # '//*[@id="searchtools"]/a[1]'
        elmBibtex = wait.until(EC.visibility_of_element_located((By.XPATH, '//*[@id="searchtools"]/a[1]')))
        # <a href="exportformats_search.cfm?query=%28dominik%20slezak%29&amp;filtered=&amp;within=owners%2Eowner%3DHOSTED&amp;dte=2008&amp;bfr=2018&amp;srt=%5Fscore&amp;expformat=csv">csv</a>
        # https://dl.acm.org/exportformats_search.cfm?query=%28dominik%20slezak%29&filtered=&within=owners%2Eowner%3DHOSTED&dte=2008&bfr=2018&srt=%5Fscore&expformat=csv
        elmBibtex.click()
        # '//*[@id="searchtools"]/a[4]'
        elmCSV = wait.until(EC.visibility_of_element_located((By.XPATH, '//*[@id="searchtools"]/a[4]')))
        elmCSV.click()
        while True:
            bib_files = glob.glob(os.path.join(folder_name_temp, '*.bib'))
            if len(bib_files) > 0:
                last_bib_file = max(bib_files, key=os.path.getctime)
                os.rename(last_bib_file, filename_bib)
                break
            time.sleep(1)
        while True:
            csv_files = glob.glob(os.path.join(folder_name_temp, '*.csv'))
            if len(csv_files) > 0:
                last_csv_file = max(csv_files, key=os.path.getctime)
                os.rename(last_csv_file, filename_csv)
                break
            time.sleep(1)
    except Exception as e:
        h.log_line('Failed for:', download_url, e)
        h.write_csv_file(None, filename_bib)
        h.write_csv_file(None, filename_csv)
        status = False
    time.sleep(5)
    browser.close()
    return status


def cookie_to_txt(c: dict):
    """domain - The domain that created AND that can read the variable.
flag - A TRUE/FALSE value indicating if all machines within a given domain can access the variable. This value is set automatically by the browser, depending on the value you set for domain.
path - The path within the domain that the variable is valid for.
secure - A TRUE/FALSE value indicating if a secure connection with the domain is needed to access the variable.
expiration - The UNIX time that the variable will expire on. UNIX time is defined as the number of seconds since Jan 1, 1970 00:00:00 GMT.
name - The name of the variable.
value - The value of the variable."""
    try:
        l = [c['domain'], str(c['domain'][0] == '.'), c['path'], c['secure'], c['expiry'], c['name'], c['value']]
        t = h.list_to_str(l, separator='\t')
        return t
    except Exception as e:
        h.log_line('Cookie not converted!', c, e)
        return ''


def download_acm_files_test(
        download_url='https://dl.acm.org/results.cfm?query=(dominik%20slezak)&within=owners.owner=HOSTED&filtered=&dte=2008&bfr=2018',
        folder_name_temp='/home/eftim/article-analysis/data/AAL_20180921/acm/temp',
        filename_bib='/home/eftim/article-analysis/data/AAL_20180921/acm/dominik-slezak.bib',
        filename_csv='/home/eftim/article-analysis/data/AAL_20180921/acm/dominik-slezak.csv'):
    start_year = 2008
    end_year = 2018
    url_keyword = 'dominik%20slezak'
    url_template_download = 'http://dl.acm.org/exportformats_search.cfm?query={0}&filtered=&within=owners%2Eowner%3DHOSTED&dte={1}&bfr={2}&srt=%5Fscore&expformat={3}'

    download_url_bib = url_template_download.format(url_keyword, start_year, end_year, 'bibtex')
    download_url_csv = url_template_download.format(url_keyword, start_year, end_year, 'csv')

    import tools
    # tools.tools.download_file_simple(download_url_bib,filename_bib)
    # tools.tools.download_file_simple(download_url_csv, filename_csv)

    # THROWS error
    # tools.tools.download_file_simple2(download_url_bib,filename_bib)
    # tools.tools.download_file_simple2(download_url_csv, filename_csv)
    # tools.tools.download_file_wget(download_url_bib,filename_bib)
    # tools.tools.download_file_wget(download_url_csv, filename_csv)
    # return
    h.log_line(download_url, 'download', filename_bib, filename_csv)
    status = True
    if os.path.isfile(filename_bib) and os.path.isfile(filename_csv) and os.stat(filename_bib).st_size > 0 and os.stat(
            filename_csv).st_size > 0:
        h.log_line('Files', filename_bib, filename_csv, 'exist. Skipping download.')
        return status
    browser = create_browser(download_dir=folder_name_temp)
    browser.get(download_url)
    wait = WebDriverWait(browser, Config.browser_delay_time_acm)
    cookies_txt = [cookie_to_txt(c) for c in browser.get_cookies()]
    cookies_filename = os.path.join(folder_name_temp, 'cookies.txt')
    h.write_csv_file(cookies_txt, cookies_filename)

    args = ['wget', '--cookies=on', '--load-cookies', cookies_filename, '-o', filename_bib + '.log', '-O', filename_bib,
            download_url_bib]
    cmd = ' '.join(args)
    # h.safe_exec_bash_cmd(args)
    print(cmd)
    args = ['wget', '--cookies=on', '--load-cookies', cookies_filename, '-o', filename_bib + '.log', '-O', filename_csv,
            download_url_csv]
    # h.safe_exec_bash_cmd(args)
    cmd = ' '.join(args)
    print(cmd)
    # return
    try:
        # elmBibtex = wait.until(EC.visibility_of_element_located((By.XPATH, "//*[@id='searchtools']/a[1]")))
        elmBibtex = wait.until(EC.visibility_of_element_located((By.PARTIAL_LINK_TEXT, "bibtex")))
        elmBibtex.click()
        while True:
            bib_files = glob.glob(os.path.join(folder_name_temp, '*.bib'))
            if len(bib_files) > 0:
                last_bib_file = max(bib_files, key=os.path.getctime)
                os.rename(last_bib_file, filename_bib)
                break
            time.sleep(1)
        # # elmCSV = wait.until(EC.visibility_of_element_located((By.XPATH, "//*[@id='searchtools']/a[4]")))
        elmCSV = wait.until(EC.visibility_of_element_located((By.PARTIAL_LINK_TEXT, "csv")))
        elmCSV.click()
        while True:
            csv_files = glob.glob(os.path.join(folder_name_temp, '*.csv'))
            if len(csv_files) > 0:
                last_csv_file = max(csv_files, key=os.path.getctime)
                os.rename(last_csv_file, filename_csv)
                break
            time.sleep(1)
    except Exception as e:
        h.log_line('Failed for:', download_url, e)
        h.write_csv_file(None, filename_bib)
        h.write_csv_file(None, filename_csv)
        status = False
    finally:
        time.sleep(5)
        browser.close()
    return status
