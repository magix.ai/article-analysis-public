from __future__ import annotations

import datetime
import json
import os
import shutil
from collections import OrderedDict

import glob
from slugify import slugify

import helpers as h
from config import Config
from tools import tools


class SearchConfig(object):
    def __init__(self, _folder: str = None, start_year: int = None, end_year: int = None,
                 folder_repo=None, relevant=1, add_lin_coef=False, apply_stemming=True, capitalize=False,
                 plot_type='pdf'):
        self._search_data = {}
        self.folder_data = _folder
        self._filename_json = 'search_data_input.json'
        self._filename_pickle = 'search_data.pickle'
        self._property_groups = None
        self.status = dict()  # set statuses of different processing steps
        self.status_long = dict()  # set statuses of different processing steps

        self.keywords = []
        # the complication is so each dict can be deserialized in a C# class for the backend
        self.properties = []  # [{"property_group":"", "synonyms":[]}]
        self.properties_mandatory = []
        self.libraries = {}
        self.properties_mandatory_stemmed = []
        self.properties_stemmed = []
        self.keyword_lookup = {}
        self.property_lookup = {}
        self.property_stem_lookup = {}
        self.property_stem_lookup_tuple = {}
        self.stem_source_lookup = {}
        self.stem_source_lookup_tuple = {}
        self.properties_major = None
        self.property_synonym_lookup = {}
        self.stemmed_properties = {}
        self.stem_to_property_map = {}
        self.processed_dois = {}
        self.processed_urls = {}
        self.result_files = set()
        self.result_counts = {}

        self.set_parameters(folder_repo=folder_repo, relevant=relevant, add_lin_coef=add_lin_coef, plot_type=plot_type,
                            capitalize=capitalize, stem=apply_stemming, start_year=start_year, end_year=end_year)
        self.set_result_counts()
        self.create_folders()
        h.log_line('Creating search object for:', self.folder_data, 'start:', self.start_year, 'end:', self.end_year)

    def set_status(self, key: str, value: str = None):
        self.status[key] = [h.timestamp(), value]
        self.status_long.setdefault(key, [])
        if value is None:
            value = 'Completed'
        self.status_long[key].append([h.timestamp(), value])
        self.save_status()

    @property
    def search_data(self):
        return self._search_data

    @search_data.setter
    def search_data(self, value):
        self._search_data = value

    def is_library_enabled(self, library_key: str):
        return self.search_data.get("libraries", {}).get(library_key.lower(), False)

    def set_library_enabled(self, library_key: str, enabled: bool = True):
        self.search_data.setdefault("libraries", {})
        if enabled is not None:
            self.search_data["libraries"][library_key] = enabled

    @property
    def apply_stemming(self):
        return self.search_data.get("apply_stemming", True)

    @apply_stemming.setter
    def apply_stemming(self, value: bool):
        if value is not None:
            self.search_data["apply_stemming"] = value

    @property
    def plot_type(self):
        s = self.search_data.get("plot_type", "pdf").lower()
        if s not in ("png", "pdf"):
            return "pdf"
        else:
            return s

    @plot_type.setter
    def plot_type(self, value: str):
        if value is not None:
            self.search_data["plot_type"] = value.lower()

    @property
    def add_lin_coef(self):
        return self.search_data.get("add_lin_coef", False)

    @add_lin_coef.setter
    def add_lin_coef(self, value: bool):
        if value is not None:
            self.search_data["add_lin_coef"] = value

    @property
    def libraries(self):
        return self.search_data["libraries"]

    @libraries.setter
    def libraries(self, value):
        if value is not None:
            self.search_data["libraries"] = value

    @property
    def keywords(self):
        return self.search_data["keywords"]

    @keywords.setter
    def keywords(self, value):
        if value is not None: self.search_data["keywords"] = value

    @property
    def capitalize(self):
        """Whether to capitalize the text in charts and figures"""
        return self.search_data.get("capitalize", False)

    @capitalize.setter
    def capitalize(self, value: bool):
        if value is not None: self.search_data["capitalize"] = value

    @property
    def properties_mandatory(self):
        return self.search_data['properties_mandatory']

    @properties_mandatory.setter
    def properties_mandatory(self, value):
        if value is not None: self.search_data['properties_mandatory'] = value

    @property
    def properties(self):
        return self.search_data['properties']

    @properties.setter
    def properties(self, value):
        if value is not None: self.search_data['properties'] = value

    @property
    def property_groups(self):
        if self._property_groups is None:
            self._property_groups = dict([(x['property_group'], x['synonyms']) for x in self.properties])
        return self._property_groups

    @property
    def start_year(self):
        return int(self.search_data.get('year_start', datetime.datetime.now().year - 10))

    @property
    def end_year(self):
        return int(self.search_data.get('year_end', datetime.datetime.now().year))

    @end_year.setter
    def end_year(self, value):
        if value is None and self.search_data.get('year_end', None) is None:
            value = datetime.datetime.now().year
        if value is not None:
            self.search_data['year_end'] = str(value)

    @start_year.setter
    def start_year(self, value):
        if self.search_data.get('year_start', None) is None and value is None:
            # 10 years, both inclusive
            value = self.end_year - 10
        if value is not None: self.search_data['year_start'] = str(value)

    @property
    def relevant(self):
        """Minimum number of relevant properties"""
        return int(self.search_data.get('relevant', Config.min_properties_relevant))

    @relevant.setter
    def relevant(self, value):
        if value is not None: self.search_data['relevant'] = str(value)

    @property
    def folder_data(self) -> str:
        return str(self._search_data['path'])

    @folder_data.setter
    def folder_data(self, _folder: str):
        if _folder is None:
            _folder = h.timestamp_short()
        if not (_folder.startswith('.') or '/' in _folder or '\\' in _folder):
            _folder = slugify(_folder.lower())
            # only folder name, so place it in the data folder
            self._search_data['path'] = os.path.join(os.getcwd(), Config.DATA_FOLDER, _folder)
        elif os.path.isdir(_folder):
            self._search_data['path'] = os.path.realpath(_folder)
        else:
            # this is relative or absolute path, so use it as is
            self._search_data['path'] = os.path.dirname(os.path.realpath(_folder))

    @property
    def folder_repo(self):
        return self.search_data.get("parth_repo",
                                    os.path.join(os.path.dirname(os.path.realpath(self.folder_data)), 'REPO'))

    @folder_repo.setter
    def folder_repo(self, value):
        if value is not None: self.search_data["parth_repo"] = value

    @property
    def project_id(self):
        return self.search_data.get("project_id", "-1")

    @project_id.setter
    def project_id(self, value):
        if value is not None: self.search_data["project_id"] = str(value)

    @property
    def project_name(self):
        return self.search_data.get("project_name", "project_name")

    @project_name.setter
    def project_name(self, value):
        if value is not None: self.search_data["project_name"] = str(value)

    @property
    def filename_initial(self):
        return os.path.join(self.folder_data, self._filename_json)

    @property
    def filename_bibtex_temp(self):
        return os.path.join(self.folder_data, 'temp_bibtex_keys.pickle')

    @property
    def folder_results(self):
        return os.path.join(self.folder_data, 'results')

    @property
    def file_articles_all(self):
        return os.path.join(self.folder_results, 'articles.csv')

    @property
    def filen_articles_bib(self):
        return os.path.join(self.folder_results, 'articles.bib')

    @property
    def file_articles_stemmed(self):
        return os.path.join(self.folder_results, 'articles_stemmed.csv')

    @property
    def file_results_property_groups(self):
        return os.path.join(self.folder_results, 'all_property_groups.csv')

    @property
    def file_results_properties(self):
        return os.path.join(self.folder_results, 'all_properties.csv')

    @property
    def filen_results_details(self):
        return os.path.join(self.folder_results, 'all_details.csv')

    @property
    def file_results_graph_data(self):
        return os.path.join(self.folder_results, 'filtered_graph_data.csv')

    @property
    def file_results_graph_plot(self):
        return os.path.join(self.folder_results, 'filtered_graph')

    @property
    def file_results_articles_filtered(self):
        return os.path.join(self.folder_results, 'filtered_articles.csv')

    @property
    def file_results_properties_filtered(self):
        return os.path.join(self.folder_results, 'filtered_properties.csv')

    @property
    def file_results_details_filtered(self):
        return os.path.join(self.folder_results, 'filtered_details.csv')

    @property
    def file_results_aggr_property_group_year(self):
        return os.path.join(self.folder_results, 'aggr_property_group_year.csv')

    @property
    def file_results_aggr_property_group_year_pivot(self):
        return os.path.join(self.folder_results, 'aggr_property_group_year_pivot.csv')

    @property
    def file_results_aggr_property_year(self):
        return os.path.join(self.folder_results, 'aggr_property_year.csv')

    @property
    def file_results_aggr_property_year_pivot(self):
        return os.path.join(self.folder_results, 'aggr_property_year_pivot.csv')

    @property
    def file_results_aggr_source_year(self):
        return os.path.join(self.folder_results, 'aggr_source_year.csv')

    @property
    def file_results_aggr_year(self):
        return os.path.join(self.folder_results, 'aggr_year.csv')

    @property
    def file_results_aggr_prisma(self):
        return os.path.join(self.folder_results, 'aggr_prisma.csv')

    @property
    def file_results_aggr_source_year_pivot_analyzed(self):
        return os.path.join(self.folder_results, 'aggr_source_year_pivot_analyzed.csv')

    @property
    def file_results_aggr_source_year_pivot_relevant(self):
        return os.path.join(self.folder_results, 'aggr_source_year_pivot_relevant.csv')

    @property
    def file_results_aggr_keyword_year_pivot_relevant(self):
        return os.path.join(self.folder_results, 'aggr_keyword_year_pivot_relevant.csv')

    @property
    def file_results_aggr_keyword_source_pivot_relevant(self):
        return os.path.join(self.folder_results, 'aggr_keyword_source_pivot_relevant.csv')

    @property
    def file_results_aggr_source(self):
        return os.path.join(self.folder_results, 'aggr_source.csv')

    @property
    def file_results_aggr_num_countries_affiliations_authors(self):
        return os.path.join(self.folder_results, 'aggr_num_countries_affiliations_authors.csv')

    @property
    def file_searched_keywords_final(self):
        return os.path.join(self.folder_results, 'searched_data.json')

    @property
    def folder_ACM(self):
        return os.path.join(self.folder_repo, Config.ACM_FOLDER)

    @property
    def folder_ACM_temp(self):
        return os.path.join(self.folder_repo, Config.ACM_FOLDER, 'temp')

    @property
    def folder_ACM_Articles(self):
        return os.path.join(self.folder_repo, Config.ACM_FOLDER, 'articles')

    @property
    def folder_IEEE_Articles(self):
        return os.path.join(self.folder_repo, Config.IEEEXPLORE_FOLDER, 'articles')

    @property
    def folder_IEEEXplore(self):
        return os.path.join(self.folder_repo, Config.IEEEXPLORE_FOLDER)

    @property
    def folder_Springer(self):
        return os.path.join(self.folder_repo, Config.SPRINGER_FOLDER)

    @property
    def folder_Springer_Links(self):
        return os.path.join(self.folder_repo, Config.SPRINGER_FOLDER, 'links')

    @property
    def folder_Springer_Articles(self):
        return os.path.join(self.folder_repo, Config.SPRINGER_FOLDER, 'articles')

    @property
    def folder_NCBI(self):
        return os.path.join(self.folder_repo, Config.NCBI_FOLDER)

    def create_folders(self):
        h.create_folder(self.folder_data)
        h.create_folder(self.folder_repo)
        h.create_folder(self.folder_results)
        h.create_folder(self.folder_ACM)
        h.create_folder(self.folder_ACM_temp)
        h.create_folder(self.folder_ACM_Articles)
        h.create_folder(self.folder_IEEEXplore)
        h.create_folder(self.folder_IEEE_Articles)
        h.create_folder(self.folder_Springer)
        h.create_folder(self.folder_Springer_Links)
        h.create_folder(self.folder_Springer_Articles)
        h.create_folder(self.folder_NCBI)

    def move_old_results(self):
        archive = os.path.join(self.folder_results, 'archive')
        h.create_folder(archive)
        patterns = ['aggr*pdf', 'aggr*png', '*prisma*', 'aggr*csv', 'aggr*xlsx', '*bib', '*txt', '*json', '*pickle',
                    'plot*', '*searched*', 'filtered_articles.xlsx', 'filtered_property_groups.xlsx',
                    'filtered_graph*spring*', 'filtered_graph_data.csv']
        files = set()
        for p in patterns:
            pfiles = glob.glob(os.path.join(self.folder_results, p))
            files.update(set(pfiles))
        for f in files:
            fn = h.get_filename(f)
            f_new = os.path.join(archive, fn)
            h.log_line(f'Trying to move from {f} to {f_new}')
            shutil.move(f, f_new)

    @property
    def filename_json(self):
        return os.path.join(self.folder_data, self._filename_json)

    @property
    def filename_pickle(self):
        return os.path.join(self.folder_data, self._filename_pickle)

    @property
    def filename_log(self):
        self.create_folders()
        return os.path.join(self.folder_results, "log.txt")

    @property
    def filename_status(self):
        return os.path.join(self.folder_results, "status.txt")

    @property
    def filename_status_long(self):
        return os.path.join(self.folder_results, "status_long.txt")

    @filename_json.setter
    def filename_json(self, _filename_initial):
        self._filename_json = _filename_initial
        fn, exten = os.path.splitext(self._filename_json)
        self._filename_pickle = fn + '_internal.pickle'

    def set_result_counts(self, sources=None):
        if not sources:
            sources = ['NCBI', 'Springer', 'ACM', 'IEEE Xplore']
        if not isinstance(sources, list):
            sources = [sources]
        for source in sources:
            self.result_counts.setdefault(source, {})
            for metric in Config.search_stats_header[:-1]:  # except 'relevant', which is calculated later
                self.result_counts[source].setdefault(metric, 0)

    def remove_doi(self, new_doi, source='ieee'):
        if new_doi in self.processed_dois:
            if source in self.processed_dois[new_doi]:
                self.processed_dois[new_doi].remove(source)
                if len(self.processed_dois[new_doi]) == 0:
                    self.processed_dois.pop(new_doi)

    def remove_dois_source(self, source='ieee'):
        self.result_counts[source] = {}
        for doi in self.processed_dois:
            if source in self.processed_dois[doi]:
                self.remove_doi(doi, source)

    def exists_doi(self, new_doi, source='ieee'):
        new_doi = h.remove_nonalphanumeric(new_doi, '')
        exists = new_doi in self.processed_dois
        self.processed_dois.setdefault(new_doi, set())
        self.processed_dois[new_doi].add(source)
        return exists

    def exists_url(self, new_url, source='ieee'):
        new_url = tools.simplify_url(new_url)
        exists = new_url in self.processed_urls
        self.processed_urls.setdefault(new_url, set())
        self.processed_urls[new_url].add(source)
        return exists

    def save_status(self):
        self.create_folders()
        h.to_json(self.status, self.filename_status, pretty_print=True)
        h.to_json(self.status_long, self.filename_status_long, pretty_print=True)

    def to_pickle(self):
        h.to_pickle(self, self.filename_pickle)

    def reset_folder_data(self, filename):
        cur_folder = os.path.dirname(os.path.realpath(filename))
        if self.folder_data != cur_folder:
            # in case this is a copy of a previous execution
            self.folder_data = cur_folder

    @staticmethod
    def from_pickle(filename) -> SearchConfig:
        d = h.from_pickle(filename)
        d.reset_folder_data(filename)
        d.create_folders()
        return d

    def reset_processed_ids(self):
        h.log_line('Resetting counters.')
        self.processed_dois = {}
        self.processed_urls = {}
        self.result_files = set()
        self.result_counts = {}

    def save_keywords(self, input_type=None):
        if input_type is not None:
            self.keywords = ['AAL and deep learning', "ELE and machine learning"]
            self.properties = [{'property_group': 'Algorithms',
                                'synonyms': [['ANN', 'NN', 'Neural Networks'], ["DL", "Deep Learning"]]},
                               {'property_group': 'Application',
                                'synonyms': [['Activity Recognition', 'ADL'], ["Time Series Analysis"]]}]
        try:
            # json_data = h.to_json_simple(self.search_data, self.filename_json)
            self.save_object(self.filename_json)
        except Exception as e:
            h.log_line('type:', self.search_data, e)
            raise e

    def reload_keywords(self, filename: str = None):
        if filename is None:
            filename = self.filename_json
        # search_data = h.from_json_simple(json_file=filename)
        search = SearchConfig.load_object(filename)
        self.search_data = search.search_data

    @staticmethod
    def load_object(filename: str) -> SearchConfig:
        def configuration_decoder(params):
            # this is some inner dict
            return params

        with open(filename, 'r', encoding='utf-8') as fin:
            search_data = json.load(fin, object_hook=configuration_decoder)
        search = SearchConfig()
        search.search_data = search_data
        search.reset_folder_data(filename)
        if filename != search.filename_json:
            search.save_object()

        search.stem_properties()
        search.set_result_counts()
        return search

    def save_object(self, filename: str = None):
        if filename is None:
            filename = self.filename_json
        with open(filename, 'w', encoding='utf-8') as fout:
            json.dump(self.search_data, fout, cls=ConfigurationEncoder, indent=4)

    def _propose_synonyms_for_keywords(self):
        self.search_data['keywords'].setdefault('proposed', [])
        # dataset.search_data.setdefault('reverse_keywords',{})
        proposed = set([k.lower() for k in self.search_data['keywords']['initial']])
        for keyword in self.search_data['keywords']['initial']:
            synonyms = [s.lower() for s in tools.get_synonyms(keyword)]
            for synonym in synonyms:
                self.keyword_lookup[synonym] = keyword
            proposed.update(set(synonyms))
        self.search_data['keywords']['proposed'] = sorted(proposed)

    def stem_properties(self):
        self.set_status("Property Stemming", "Starting")
        self.properties_stemmed = {}
        properties_major = OrderedDict()
        self.properties_mandatory_stemmed = []
        for properties_mandatory_OR_conditions in self.properties_mandatory:
            # e.g. properties_mandatory_conditions= ['nlp', 'natural language processing'], both are synonyms, so OR
            properties_mandatory_conditions_tuples = []
            for property_mandatory in properties_mandatory_OR_conditions:
                h.log_line(property_mandatory)
                stemmed_property_mandatory = tools.stem_text(property_mandatory, self.apply_stemming)
                h.log_line(property_mandatory, stemmed_property_mandatory)
                properties_mandatory_conditions_tuples.extend(
                    tools.text_to_tuple_permutations(stemmed_property_mandatory))
            self.properties_mandatory_stemmed.append(list(set(properties_mandatory_conditions_tuples)))
        for property_group, synonym_groups in self.property_groups.items():
            stemmed_synonyms = set()
            for synonyms in synonym_groups:
                if not synonyms or len(synonyms) == 0:
                    continue
                synonym_major = synonyms[0]
                properties_major[synonym_major] = None
                # This allows: wood-steel to be treated as: woodsteel, wood steel, and later on, steel wood
                synonyms_internal = synonyms + [s.replace('-', ' ') for s in synonyms if '-' in s]
                for synonym in synonyms_internal:
                    stemmed_synonym = tools.stem_text(synonym, self.apply_stemming)
                    stemmed_synonym_tuples = tools.text_to_tuple_permutations(stemmed_synonym)
                    for stemmed_synonym_tuple in stemmed_synonym_tuples:
                        self.property_stem_lookup_tuple.setdefault(stemmed_synonym_tuple, set())
                        self.property_stem_lookup_tuple[stemmed_synonym_tuple].add(property_group)
                        self.stem_source_lookup_tuple[stemmed_synonym_tuple] = synonym
                    self.property_lookup.setdefault(synonym, set())
                    self.property_lookup[synonym].add(property_group)
                    self.property_stem_lookup.setdefault(stemmed_synonym, set())
                    self.property_stem_lookup[stemmed_synonym].add(property_group)

                    self.property_synonym_lookup.setdefault(synonym, set())
                    self.property_synonym_lookup[synonym].add(synonym_major)
                    self.stem_source_lookup[stemmed_synonym] = synonym
                    stemmed_synonyms.add(stemmed_synonym)
            self.properties_stemmed[property_group] = sorted(stemmed_synonyms)
        self.properties_major = list(properties_major.keys())
        self.set_status("Property Stemming")
        self.to_pickle()

    def write_results(self, data, filename):
        self.result_files.add(filename)
        if data is None:
            data = []
        if len(data) > 0 and data[0] != Config.keywords_result_header:
            data = [Config.keywords_result_header] + data
        h.write_csv_file(data, filename, separator='\t')
        self.to_pickle()

    def set_parameters(self, ieee=True, pmc=None, pubmed=None, springer=None, acm=None, folder_repo=None,
                       relevant=None, add_lin_coef=False, plot_type='pdf', capitalize=False, stem=True, start_year=None,
                       end_year=None, elsevier=None, mdpi=None):
        self.project_id = -1
        self.project_name = "MAGIX.AI"
        self.folder_repo = folder_repo
        self.relevant = relevant
        self.add_lin_coef = add_lin_coef
        self.apply_stemming = stem
        self.plot_type = plot_type
        self.capitalize = capitalize
        self.end_year = end_year
        self.start_year = start_year
        self.set_library_enabled("ieee", ieee)
        self.set_library_enabled("pmc", pmc)
        self.set_library_enabled("pubmed", pubmed)
        self.set_library_enabled("springer", springer)
        self.set_library_enabled("acm", acm)
        self.set_library_enabled("elsevier", elsevier)
        self.set_library_enabled("mdpi", mdpi)
        self.create_folders()
        h.setup_logging(output_filename=self.filename_log)

    @staticmethod
    def prepare_search_object(filename: str, ieee=None, pmc=None, pubmed=None, springer=None, acm=None,
                              folder_repo=None,
                              relevant=None, add_lin_coef=False, plot_type='pdf', capitalize=False, stem=True,
                              start_year=None,
                              end_year=None) -> SearchConfig:
        if filename.lower().endswith('pkl') or filename.lower().endswith('pickle'):
            search = SearchConfig.from_pickle(filename)
        elif filename.lower().endswith('json'):
            search = SearchConfig.load_object(filename)
        else:
            raise Exception("Filetype not supported:" + filename)
        search.set_parameters(ieee=ieee, pmc=pmc, pubmed=pubmed, springer=springer, acm=acm,
                              folder_repo=folder_repo, relevant=relevant, add_lin_coef=add_lin_coef,
                              plot_type=plot_type, capitalize=capitalize, stem=stem,
                              start_year=start_year, end_year=end_year)
        return search


class ConfigurationEncoder(json.JSONEncoder):
    def default(self, obj):
        config_params = dict()
        config_params['__type__'] = obj.__class__.__name__
        config_params.update(obj.__dict__)
        return config_params
