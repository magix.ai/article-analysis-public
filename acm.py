from tools import tools
from config import *
import urllib
import helpers as h
from search_config import SearchConfig
from slugify import slugify
import csv, re, html
import re
from ieeexplore import ieeexplore
from mitpress import mitpress
import lxml
from urllib.parse import urlparse
import pathlib
from browser_emulate import create_browser, download_file_selenium
from time import sleep
from selenium import webdriver
from selenium.webdriver.common import action_chains, keys
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from countries import Countries
import time

class acm(object):
    url_template_download = 'http://dl.acm.org/exportformats_search.cfm?query={0}&filtered=&within=owners%2Eowner%3DHOSTED&dte={1}&bfr={2}&srt=%5Fscore&expformat={3}'
    # https://dl.acm.org/results.cfm?query=(eftim%20zdravevski)&within=owners.owner=HOSTED&filtered=&dte=1947&bfr=2018
    url_template_search = 'https://dl.acm.org/results.cfm?query=({0})&within=owners.owner=HOSTED&filtered=&dte={1}&bfr={2}'

    _doi_xpaths = ['//*[@id="divmain"]/table[2]/tbody/tr/td/table/tbody/tr[5]/td/span[5]/a',
                   '//*[@id="divmain"]/table[2]/tbody/tr/td/table/tbody/tr[5]/td/span[4]/a',
                   '//*[@id="divmain"]/table[2]/tbody/tr/td/table/tbody/tr[5]/td/span[3]/a',
                   '//*[@id="divmain"]/table[2]/tbody/tr/td/table/tbody/tr[4]/td/span[4]/a',
                   '//*[@id="divmain"]/table[2]/tbody/tr/td/table/tbody/tr[5]/td/span/a',
                   '//*[@id="divmain"]/table/tbody/tr/td[1]/table[3]/tbody/tr/td/table/tbody/tr[5]/td/span[4]/a',
                   '//*[@id="divmain"]/table/tbody/tr/td[1]/table[3]/tbody/tr/td/table/tbody/tr[4]/td/span[5]/a',
                   '//*[@id="divmain"]/table/tbody/tr/td[1]/table[3]/tbody/tr/td/table/tbody/tr[4]/td/span[4]/a'
                   ]

    _authors_affiliations_xpaths = ['//*[@id="divmain"]/table/tbody/tr/td[1]/table[2]/tbody',
                                    '//*[@id="divmain"]/table[1]/tbody/tr/td[1]/table[2]/tbody',
                                    ]
    _affil_xpath = '//*[@id="divmain"]/table[1]/tbody/tr/td[1]/table[2]/tbody/tr'
    _abstract_xpath = '//*[@id="cf_layoutareaabstract"]/div/div/p'
    _abstract_xpath_general = '//*[@id="cf_layoutareaabstract"]'

    @staticmethod
    def crawl(search: SearchConfig, keyword: str = None):
        search.set_status('ACM', 'Starting')
        h.log_line('Crawling ACM Xplore...')
        search.result_counts[Config.sources['acm']] = {}
        if keyword:
            acm._process_keyword(search, keyword)
        else:
            for keyword in search.keywords:
                acm._process_keyword(search, keyword)
        search.set_status('ACM')
        search.to_pickle()

    @staticmethod
    def _process_keyword(search: SearchConfig, keyword: str):
        search.set_result_counts(Config.sources['acm'])

        filename_result_pages = os.path.join(search.folder_ACM_Articles, slugify(keyword) + '_result_pages.csv')
        filename_articles_partial = os.path.join(search.folder_ACM_Articles, slugify(keyword) + '_articles_partial.csv')
        url_keyword = urllib.parse.quote(keyword)
        search_url = acm.url_template_search.format(url_keyword, search.start_year, search.end_year)
        result_pages_files = acm._crawl_acm_results(search_url, search.folder_ACM_Articles, filename_result_pages)
        acm._get_info_from_pages(result_pages_files, search.folder_ACM_Articles, filename_articles_partial, search_url)
        acm._get_additional_info_for_file(search, filename_articles_partial, search.folder_ACM_Articles, keyword)

    @staticmethod
    def _get_bases_from_url(url, download_dir):
        parsed_uri = urlparse(url)
        base_url = '{uri.scheme}://{uri.netloc}/'.format(uri=parsed_uri)
        local_base_url = pathlib.Path(download_dir).as_uri() + '/'
        return base_url, local_base_url

    @staticmethod
    def _crawl_acm_results(url, download_dir, filename_csv):
        base_url, local_base_url = acm._get_bases_from_url(url, download_dir)

        downloaded_filenames = []

        browser = create_browser(download_dir)
        filename = tools.html_filename(url, download_dir)
        web_page, final_url = download_file_selenium(url, download_dir, filename)
        downloaded_filenames.append(filename)
        browser.get(pathlib.Path(filename).as_uri())
        wait = WebDriverWait(browser, Config.browser_delay_time_acm)

        wait.until(EC.presence_of_element_located((By.XPATH, '//*[@id="results"]/div[2]/span/a')))
        current_page = 1
        while True:
            pages = browser.find_elements_by_xpath('//*[@id="results"]/div[2]/span/a')
            found = None
            for page in pages:
                if int(page.text) > current_page:
                    current_page = int(page.text)
                    found = True
                    next_page_url = page.get_attribute('href').replace(local_base_url, base_url)
                    filename = tools.html_filename(next_page_url, download_dir)
                    web_page, final_url = download_file_selenium(next_page_url, download_dir, filename)
                    downloaded_filenames.append(filename)
                    browser.get(pathlib.Path(filename).as_uri())
                    break
            if not found:
                h.log_line("No more pages to process. Initial URL:", url)
                break
        h.write_csv_file(downloaded_filenames, filename_csv)
        browser.close()
        return downloaded_filenames

    @staticmethod
    def _get_info_from_pages(pages_local_files, download_dir, filename_csv, first_url):
        base_url, local_base_url = acm._get_bases_from_url(first_url, download_dir)
        header = ('title', 'publication_title', 'publication_date', 'publication_year', 'authors', 'download_count',
                  'cited_count', 'link')
        info_list = [header]
        articles = 0
        num_page = 0
        try:
            browser = create_browser(download_dir)
            wait = WebDriverWait(browser, Config.browser_delay_time_acm)
            for page in pages_local_files:
                num_page += 1
                h.log_line('Processing page:', num_page, 'out of maximum', Config.number_of_pages_acm,
                           'Found articles:',
                           articles)
                if num_page > Config.number_of_pages_acm:
                    h.log_line('Reached maximum number of pages:', Config.number_of_pages_acm)
                    break
                browser.get(pathlib.Path(page).as_uri())
                wait.until(EC.presence_of_element_located((By.CLASS_NAME, 'details')))
                article_links = browser.find_elements_by_class_name('details')

                for article_link in article_links:
                    if articles > Config.max_papers_acm:
                        h.log_line('Terminating ACM crawling. Max reached:', Config.max_papers_acm)
                        break
                    articles += 1
                    title = article_link.find_element_by_class_name('title')
                    article_title = title.text
                    link = title.find_element_by_tag_name('a').get_attribute('href')
                    article_url = link.replace(local_base_url, base_url)

                    try:
                        authors = article_link.find_element_by_class_name('authors').text
                    except NoSuchElementException as e:
                        authors = ""

                    try:
                        source = article_link.find_element_by_class_name('source')
                        publication_date = source.find_element_by_class_name('publication_date')
                        if publication_date:
                            publication_year = [int(s) for s in publication_date.text.split() if s.isdigit()]
                            publication_year = publication_year[0]
                        publication_date = publication_date.text
                    except NoSuchElementException as e:
                        publication_year = None
                        publication_date = None

                    try:
                        publication_title = source.find_element_by_xpath('span[2]').text
                    except NoSuchElementException as e:
                        publication_title = None

                    try:
                        cited_count = article_link.find_element_by_class_name('cited_count')
                        if cited_count:
                            cited_count = [int(s.replace(',', '')) for s in cited_count.text.split() if
                                           s.replace(',', '').isdigit()]
                            if len(cited_count) > 0:
                                cited_count = cited_count[0]
                            else:
                                cited_count = None
                    except NoSuchElementException as e:
                        cited_count = None

                    try:
                        download_count = article_link.find_element_by_class_name('downloadAll')
                        if download_count:
                            download_count = [int(s.replace(',', '')) for s in download_count.text.split() if
                                              s.replace(',', '').isdigit()]
                            if len(download_count) > 0:
                                download_count = download_count[0]
                            else:
                                download_count = None
                    except NoSuchElementException as e:
                        download_count = None

                    row = (
                        article_title, publication_title, publication_date, publication_year, authors, download_count,
                        cited_count, article_url)
                    row = [str(r).strip() for r in row]
                    info_list.append(row)

            browser.close()
        except Exception as e:
            h.log_line('Exception after some time. Current pages:', num_page, 'Current articles:', articles, ex)
        h.write_csv_file(info_list, filename_csv, separator='\t')

    @staticmethod
    def _get_additional_info_for_file(search: SearchConfig, filename_partial, download_dir, keyword):
        data, header = h.read_csv_file(filename_partial, datatype='str', separator='\t', has_header=True,
                                       separate_header=True)
        artilce_details = []
        browser = create_browser(download_dir)
        wait = WebDriverWait(browser, Config.browser_delay_time_acm)
        for i, item in enumerate(data):
            article_title, publication_title, publication_date, publication_year, authors1, download_count, cited_count, article_url = item
            try:
                filename = tools.html_filename(article_url, download_dir)
                web_page, final_url = download_file_selenium(article_url, download_dir, filename,
                                                             '//*[@class="tabbody"]')
                browser.get(pathlib.Path(filename).as_uri())

                wait.until(EC.presence_of_element_located((By.XPATH, '//*[@class="tabbody"]')))
                try:
                    abstract = browser.find_element_by_xpath('//*[@id="cf_layoutareaabstract"]/div/div/p').text.strip()
                except NoSuchElementException as e:
                    abstract = ''

                doi = None
                for xp in acm._doi_xpaths:
                    try:
                        doi = browser.find_element_by_xpath(xp).text
                    except NoSuchElementException as e:
                        doi = None
                        continue
                    if doi: break
                authors_affilations = None
                try:
                    authors2 = None
                    authors_affilations_wrapper = browser.find_elements_by_xpath(
                        '//*[@id="divmain"]/table[1]/tbody/tr/td[1]/table[2]/tbody/tr')
                    if authors_affilations_wrapper:
                        author_list = []
                        affilation_list = []
                        for author_affilation in authors_affilations_wrapper:
                            author = author_affilation.find_element_by_tag_name('a')
                            affilation = author_affilation.find_element_by_tag_name('small').replace(';', ',')
                            author_list.append(author.text.replace(',', ' '))
                            affilation_list.append(affilation.text)
                        authors2 = ','.join(author_list)
                        authors_affilations = ';'.join(affilation_list)
                except Exception as e:
                    authors2 = None
                # authors = authors1 if len(authors1) > len(authors2) else authors2
                authors = authors2
                num_authors = authors.count(',') + 1
                s_emails = ''
                affiliations, num_diff_affiliations, countries, num_diff_countries = Countries.process_affiliations(
                    authors_affilations)
                row = [doi, article_url, article_title, authors, publication_date, publication_year, cited_count,
                       abstract,
                       keyword, Config.sources['acm'], publication_title, authors_affilations, num_diff_affiliations, countries,
                       num_diff_countries, num_authors, s_emails]
                if search.exists_doi(doi, Config.sources['acm']):
                    search.result_counts[Config.sources['acm']]['duplicates'] += 1
                    continue
                if search.exists_url(article_url, Config.sources['acm']):
                    search.result_counts[Config.sources['acm']]['duplicates'] += 1
                    continue
                if len(abstract) < Config.abstract_min_length:
                    search.result_counts[Config.sources['acm']]['incomplete_data'] += 1
                    continue
                if h.is_empty(authors):
                    search.result_counts[Config.sources['acm']]['incomplete_data'] += 1
                    continue
                artilce_details.append(row)
            except Exception as e:
                h.log_line("Couldn't download url:", article_url, 'article', i, 'from', len(data))
        browser.close()
        filename_final = os.path.join(search.folder_ACM, slugify(keyword) + '_links.csv')
        search.write_results(data, filename_final)
