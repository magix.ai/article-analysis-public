#!/usr/bin/env bash

cat << GL_PRIVKEY > ~/.ssh/gitlab_magix.pem
-----BEGIN RSA PRIVATE KEY-----
ENTER YOUR KEY HERE
-----END RSA PRIVATE KEY-----
GL_PRIVKEY
chmod 400 ~/.ssh/gitlab_magix.pem

cat << CONFIG >> ~/.ssh/config
Host gitlab.com
 AddKeysToAgent yes
 StrictHostKeyChecking no
 UserKnownHostsFile /dev/null
 IdentityFile ~/.ssh/gitlab_magix.pem
CONFIG
chmod 400 ~/.ssh/config

eval `ssh-agent`
ssh-add ~/.ssh/gitlab_magix.pem

# https://github.com/git-lfs/git-lfs/wiki/Installation
sudo apt-get install software-properties-common
sudo apt install texlive-extra-utils
sudo add-apt-repository ppa:git-core/ppa
curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | sudo bash
sudo apt-get --yes install git-lfs
git lfs install

git config --global user.email "zeftim@gmail.com"
git config --global user.name "Eftim Zdravevski"

sudo apt-get update
sudo apt-get --yes install python3-pip
sudo apt-get --yes install python3-selenium
sudo apt-get --yes install python3.6-tk
sudo apt install yum
sudo apt install zip

git clone git@gitlab.com:magix.ai/article-analysis.git ~/crawler/article-analysis
git clone git@gitlab.com:magix.ai/survey-results.git ~/crawler/survey-results

pip3 install -r ~/crawler/article-analysis/requirements.txt

cat << NLTK | python3.6
#!/usr/bin/python
import nltk
nltk.download('stopwords')
nltk.download('wordnet')
nltk.download('punkt')
nltk.download('averaged_perceptron_tagger')
NLTK

sh ~/crawler/article-analysis/install_selenium_ubuntu.sh
