from tools import tools
from config import *
import urllib
import helpers as h
from search_config import SearchConfig
from slugify import slugify
import csv, re, html
import re

class mitpress(object):
    source = 'MIT Press'
    re_paragraph = re.compile(r'<p[^>]+>(.*)</p>')
    @staticmethod
    def get_abstract_mitpress(web_page):
        """Works for pages downloade from urls like https://www.mitpressjournals.org/doi/10.1162/evco.1994.2.4.321"""
        abstract = ''
        s = tools.between(web_page, 'abstractInFull', '</div>')
        s = h.remove_tabs(s)
        m = acm.re_paragraph.search(s)
        if m:
            abstract = m.group(1)
            abstract = html.unescape(abstract)
        return abstract