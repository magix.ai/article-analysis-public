from browser_emulate import *
import helpers as h
import csv
from selenium.webdriver.common.keys import Keys
from time import sleep
from selenium.common.exceptions import NoSuchElementException
from tools import tools
from lxml import html
from urllib.parse import urlparse
import pathlib


def get_bases_from_url(url, download_dir):
    parsed_uri = urlparse(url)
    base_url = '{uri.scheme}://{uri.netloc}/'.format(uri=parsed_uri)
    local_base_url = pathlib.Path(download_dir).as_uri() + '/'
    return base_url, local_base_url


_doi_xpaths = ['//*[@id="divmain"]/table[2]/tbody/tr/td/table/tbody/tr[5]/td/span[5]/a',
               '//*[@id="divmain"]/table[2]/tbody/tr/td/table/tbody/tr[5]/td/span[4]/a',
               '//*[@id="divmain"]/table[2]/tbody/tr/td/table/tbody/tr[5]/td/span[3]/a',
               '//*[@id="divmain"]/table[2]/tbody/tr/td/table/tbody/tr[4]/td/span[4]/a',
               '//*[@id="divmain"]/table[2]/tbody/tr/td/table/tbody/tr[5]/td/span/a',
               '//*[@id="divmain"]/table/tbody/tr/td[1]/table[3]/tbody/tr/td/table/tbody/tr[5]/td/span[4]/a',
               '//*[@id="divmain"]/table/tbody/tr/td[1]/table[3]/tbody/tr/td/table/tbody/tr[4]/td/span[5]/a',
               '//*[@id="divmain"]/table/tbody/tr/td[1]/table[3]/tbody/tr/td/table/tbody/tr[4]/td/span[4]/a'
               ]


def download_file(url, download_dir, filename=None, wait_xpath='//body', return_string=False):
    if filename is None:
        filename = tools.html_filename(url)
    final_url = url
    if os.path.isfile(filename) and os.stat(filename).st_size > 0:
        h.log_line('File exists:', filename, 'Skipping download of', url)
        with open(filename, 'r', encoding="utf-8") as file:
            s = file.read()
    else:
        h.log_line('Downloading', url, 'to', filename)
        browser = create_browser(download_dir)
        browser.get(url)
        wait = WebDriverWait(browser, Config.browser_delay_time_acm)
        wait.until(EC.presence_of_element_located((By.XPATH, wait_xpath)))
        s = browser.page_source
        with open(filename, 'w+', encoding="utf-8") as f:
            f.write(s)
        browser.close()
    if return_string:
        web_page = s
    else:
        web_page = html.fromstring(s)
    return web_page, final_url


def download_pages_for_url(url, download_dir):
    base_url, local_base_url = get_bases_from_url(url, download_dir)

    downloaded_filenames = []

    browser = create_browser(download_dir)
    filename = tools.html_filename(url, download_dir)
    web_page, final_url = download_file(url, download_dir, filename)
    downloaded_filenames.append(filename)
    browser.get(pathlib.Path(filename).as_uri())
    wait = WebDriverWait(browser, Config.browser_delay_time_acm)

    wait.until(EC.presence_of_element_located((By.XPATH, '//*[@id="results"]/div[2]/span/a')))
    current_page = 1
    while True:
        pages = browser.find_elements_by_xpath('//*[@id="results"]/div[2]/span/a')
        found = None
        for page in pages:
            if int(page.text) > current_page:
                current_page = int(page.text)
                found = True
                next_page_url = page.get_attribute('href').replace(local_base_url, base_url)
                filename = tools.html_filename(next_page_url, download_dir)
                web_page, final_url = download_file(next_page_url, download_dir, filename)
                downloaded_filenames.append(filename)
                browser.get(pathlib.Path(filename).as_uri())
                break
        if found == None:
            print("done with pages")
            break
    browser.close()

    return downloaded_filenames


def get_info_from_pages(pages, download_dir, filename_csv):
    base_url, local_base_url = get_bases_from_url(url, download_dir)
    header = (
    'title', 'publicationTitle', 'publicationDate', 'publicationYear', 'authors', 'downloadCount', 'citedCount', 'link')
    infoList = []
    infoList.append(header)

    browser = create_browser(download_dir)
    wait = WebDriverWait(browser, Config.browser_delay_time_acm)
    for page in pages:
        browser.get(pathlib.Path(page).as_uri())
        wait.until(EC.presence_of_element_located((By.CLASS_NAME, 'details')))
        elements = browser.find_elements_by_class_name('details')

        for element in elements:
            title = element.find_element_by_class_name('title')
            print('\ntitle: ', title.text)
            link = title.find_element_by_tag_name('a').get_attribute('href')
            element_url = link.replace(local_base_url, base_url)

            try:
                authors = element.find_element_by_class_name('authors').text
            except NoSuchElementException as e:
                authors = ""

            try:
                source = element.find_element_by_class_name('source')
                publicationDate = source.find_element_by_class_name('publicationDate')
                if publicationDate:
                    publicationYear = [int(s) for s in publicationDate.text.split() if s.isdigit()]
                    publicationYear = publicationYear[0]
                publicationDate = publicationDate.text
            except NoSuchElementException as e:
                publicationYear = None
                publicationDate = None

            try:
                publicationTitle = source.find_element_by_xpath('span[2]').text
            except NoSuchElementException as e:
                publicationTitle = None

            try:
                citedCount = element.find_element_by_class_name('citedCount')
                if citedCount:
                    citedCount = [int(s.replace(',', '')) for s in citedCount.text.split() if
                                  s.replace(',', '').isdigit()]
                    if len(citedCount) > 0:
                        citedCount = citedCount[0]
                    else:
                        citedCount = None
            except NoSuchElementException as e:
                citedCount = None

            try:
                downloadCount = element.find_element_by_class_name('downloadAll')
                if downloadCount:
                    downloadCount = [int(s.replace(',', '')) for s in downloadCount.text.split() if
                                     s.replace(',', '').isdigit()]
                    if len(downloadCount) > 0:
                        downloadCount = downloadCount[0]
                    else:
                        downloadCount = None
            except NoSuchElementException as e:
                downloadCount = None

            row = (title.text, publicationTitle, publicationDate, publicationYear, authors, downloadCount, citedCount,
                   element_url)
            infoList.append(row)

    browser.close()
    file = download_dir + filename_csv
    csv.register_dialect('myDialect', lineterminator='\n')
    with open(file, 'w+', encoding="utf-8") as csvFile:
        writer = csv.writer(csvFile, dialect='myDialect')
        writer.writerows(infoList)

    return file


def get_additional_info_for_file(file, download_dir, filename_csv):
    with open(file, 'r', encoding='utf-8') as csvFile:
        data = [tuple(line) for line in csv.reader(csvFile)]
    headers = data[0]
    headers_extended = headers + ('abstract', 'doi', 'authors2', 'authors_affilations')
    link_index = headers.index('link')
    data = data[1:]
    info_extended_list = []
    info_extended_list.append(headers_extended)

    browser = create_browser(download_dir)
    wait = WebDriverWait(browser, Config.browser_delay_time_acm)
    for item in data:
        element_url = item[link_index]
        filename = tools.html_filename(element_url, download_dir)
        web_page, final_url = download_file(element_url, download_dir, filename, '//*[@class="tabbody"]')

        browser.get(pathlib.Path(filename).as_uri())

        wait.until(EC.presence_of_element_located((By.XPATH, '//*[@class="tabbody"]')))
        try:
            abstract = browser.find_element_by_xpath('//*[@id="cf_layoutareaabstract"]/div/div/p').text
        except NoSuchElementException as e:
            abstract = 'An abstract is not available.'

        doi = None
        for xp in _doi_xpaths:
            try:
                doi = browser.find_element_by_xpath(xp).text
            except NoSuchElementException as e:
                doi = None
                continue
            if doi: break

        try:
            authors2 = None
            authors_affilations = None
            authors_affilations_wrapper = browser.find_elements_by_xpath(
                '//*[@id="divmain"]/table[1]/tbody/tr/td[1]/table[2]/tbody/tr')
            if authors_affilations_wrapper:
                author_list = []
                affilation_list = []
                for author_affilation in authors_affilations_wrapper:
                    author = author_affilation.find_element_by_tag_name('a')
                    affilation = author_affilation.find_element_by_tag_name('small')
                    author_list.append(author.text)
                    affilation_list.append(affilation.text)
                authors2 = ';'.join(author_list)
                authors_affilations = ';'.join(affilation_list)
        except Exception as e:
            authors2 = None
            authors_affilations = None

        row = item + (abstract, doi, authors2, authors_affilations)
        info_extended_list.append(row)
    browser.close()

    file_extended = download_dir + filename_csv
    csv.register_dialect('myDialect', lineterminator='\n')
    with open(file_extended, 'w+', encoding="utf-8") as csvFile:
        writer = csv.writer(csvFile, dialect='myDialect')
        writer.writerows(info_extended_list)

    return file_extended


url = 'https://dl.acm.org/results.cfm?query=(vladimir%20trajkovik)&within=owners.owner=HOSTED&filtered=&dte=2017&bfr=2018'
download_dir = 'C:/Users/Dare/Desktop/crawler/simona_data/temp/'
csvFileName = 'vladimir.csv'
filename_csv = 'extended.csv'

pages = download_pages_for_url(url, download_dir)
file = get_info_from_pages(pages, download_dir, csvFileName)
file_extended = get_additional_info_for_file(file, download_dir, filename_csv)
